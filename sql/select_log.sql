select distinct l.registry,
				(select name from users where registry = u.registry) as name,
				(select section from section where id_section = s.id_section) as section,
				(select action from action where id_action = a.id_action) as action,
				l.message,
				(date_format(l.date,'%d/%m/%Y -- %T')) as date
from log
		 inner join log l
		 inner join action a on l.id_action = a.id_action
		 inner join section s on l.id_section = s.id_section
		 inner join users u on l.registry = u.registry
where date_format(l.date,'%d/%m/%Y') = '24/05/2019' and a.id_action = 3
order by l.id_log desc;