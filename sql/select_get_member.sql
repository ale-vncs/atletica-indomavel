select distinct us.id_user,
				us.registry,
				us.name,
				us.email,
				ust.name as type,
				us.enabled,
				(select gender from gender where gender.id_gender = us.id_gender) as gender,
				ifnull(group_concat((select name from sport where id_sport = usp.id_sport group by sp.name)), 'Sem Esporte') as sport,
				ifnull((select name from class where id_class = us.class group by cl.name), 'Sem Curso') as class,
				date_format(us.birthday_date,'%d/%m/%Y') as birthday_date,
				date_format(us.join_date,'%d/%m/%Y -- %T') as join_date,
				date_format(us.association_date,'%d/%m/%Y') as association_date,
				date_format(date_add(us.association_date, interval 1 year ), '%d/%m/%Y') as expire_date,
				if(datediff(date_add(us.association_date, interval 1 year ), association_date) < 0, 'Não', 'Sim') as association
from users us
		 left join user_sport usp on us.id_user = usp.id_user
		 left join class cl ot sp on usp.id_sport = sp.id_sport
		 left joinn us.class = cl.id_class
	left join spor user_type ust on us.type = ust.id_type
where us.is_admin = 0
group by us.registry order by us.name