use u406455635_atlet;

-- =========================================================================
insert into gender (gender) values
	('Masculino'),
	('Feminino'),
	('Outros');

-- =========================================================================
insert into action (action) value
    ('Adição'), -- Adicionar
	('Remoção'), -- Remover
	('Modificação'), -- Modificar
	('Acesso'); -- Acessar

-- =========================================================================
insert into section (section) value
    ('Início'),
	('Galeria'),
	('Diretores'),
	('Membros'),
	('Avisos'),
	('Associação'),
	('Formulário'),
	('Esporte'),
	('Cursos'),
	('Sobre'),
	('Dados da Conta'),
	('Parceiros');

-- =========================================================================
insert into sport (name) value
    ('Vôlei'),
    ('Futebol'),
    ('Basquete');

-- =========================================================================
insert into course (name) value
    ('Engenharia de Computação'),
    ('Engenharia de Produção'),
    ('Ciência da Computação'),
    ('Administração'),
    ('Publicidade');

-- =========================================================================
insert into user_type (name) value
    ('Atleta'),
    ('Membros');

-- =========================================================================
insert into admin_type (name) value
	('Presidente'),
	('Diretor(a) de Futebol'),
	('Diretor(a) de Vôlei');

-- =========================================================================
insert into about (name, about_text, title_index, sub_title_index, enabled_association, date_expiration) value
    ('Atlética Indomável', 'Ainda não tem', 'Atlética Indomável','Bem - vindos', 0, current_time);

-- =========================================================================
insert into users (registry, name, email, id_gender, birthday_date, join_date, id_type, id_course, association_date) value
    ('00000000','admin','admin',3, '1997-02-18', current_timestamp(), 1, 1, current_timestamp()),
    ('00000001','Gustavo Santos Rocha','GustavoSantosRocha@armyspy.com',1, '1940-04-21', current_timestamp(), 1, 1, current_timestamp()),
    ('00000002','Nicole Correia Pinto','NicoleCorreiaPinto@rhyta.com',2, '2000-04-27', current_timestamp(), 1, 2, current_timestamp()),
    ('00000003','Victor Santos Costa','VictorSantosCosta@armyspy.com',1, '1967-10-18', current_timestamp(), 1, 3, current_timestamp()),
    ('00000004','Ryan Rocha Costa','RyanRochaCosta@armyspy.com',1, '1977-06-23', current_timestamp(), 1, 4, current_timestamp()),
    ('00000005','Isabela Ribeiro Castro','IsabelaRibeiroCastro@armyspy.com',2, '1940-01-06', current_timestamp(), 1, 4, current_timestamp()),
    ('00000006','Martim Almeida Barbosa','MartimAlmeidaBarbosa@jourrapide.com',1, '1934-03-13', current_timestamp(), 1, 4, current_timestamp()),
    ('00000007','Livia Cardoso Gomes','LiviaCardosoGomes@armyspy.com',2, '1999-11-12', current_timestamp(), 1, 1, current_timestamp()),
    ('00000008','Rafaela Oliveira Araujo','RafaelaOliveiraAraujo@jourrapide.com',2, '1990-12-11', current_timestamp(), 1, 1, current_timestamp()),
    ('00000009','Melissa Araujo Azevedo','MelissaAraujoAzevedo@jourrapide.com',2, '1967-01-24', current_timestamp(), 1, 1, current_timestamp()),
    ('00000010','Diogo Costa Pinto','DiogoCostaPinto@jourrapide.com',1, '1949-11-17', current_timestamp(), 1, 1, current_timestamp());

-- =========================================================================
insert into user_admin (id_user, id_type) value
    (1, 1);

update users set is_admin = 1 where id_user = 1;
-- =========================================================================
insert into user_attempts_login (id_user) value
    (1);