create database if not exists u406455635_atlet;
use u406455635_atlet;

create table if not exists partner( -- Armazena os parceiros
						id_partner int primary key auto_increment,
						name varchar(50) not null ,
						about text not null,
						path varchar(50) default 'unknown_partner.png'
);

create table if not exists gallery_img( -- local das imagens da galeria
							id_img bigint primary key auto_increment ,
							path varchar(50)
);

create table if not exists user_type( -- Se é atleta, associado
						  id_type int primary key auto_increment ,
						  name varchar(25)
);

create table if not exists admin_type( -- Tipo de admin
						  id_type int primary key auto_increment ,
						  name varchar(25)
);

create table if not exists section( -- Menus do dashborad, inicio, galeria, form
						id_section int primary key auto_increment ,
						section varchar(50)
);

create table if not exists action( -- Ação do usuario, deletar, adicionar, modificar
					   id_action int primary key auto_increment ,
					   action varchar(50)
);

create table if not exists gender( -- genero do usuario
					   id_gender int primary key auto_increment ,
					   gender varchar(20)
);

create table if not exists about( -- Texto sobre a atletica
					  name varchar(50),
					  about_text text not null ,
					  title_index varchar(50) not null ,
					  sub_title_index varchar(50) not null ,
					  enabled_association boolean not null ,
					  date_expiration date not null,
					  color_primary varchar(10) default '#B7153E',
					  color_secondary varchar(10) default '#2C2C2C',
					  color_text varchar(10) default '#f7f7f7'
);

create table if not exists sport( -- Os esportes praticados na atletica
					  id_sport int primary key auto_increment ,
					  name varchar(50) not null
);


create table if not exists course( -- Nome das turmas
					  id_course int primary key auto_increment ,
					  name varchar(50) not null
);

create table if not exists users( -- Cadastro de todos os participantes da atletica
					  id_user int primary key auto_increment ,
					  registry varchar(8) not null unique ,
					  name varchar(50) not null ,
					  email varchar(50) not null ,
					  id_gender int ,
					  birthday_date date not null,
					  association_date datetime not null,
					  join_date datetime default current_timestamp,
					  id_type int ,
					  id_course int,
					  is_admin boolean default false,
					  path varchar(50) default 'unknown_people.png',
					  enabled boolean default false,
					  foreign key (id_gender) references gender(id_gender) on delete set null,
					  foreign key (id_type) references user_type(id_type) on delete set null,
					  foreign key (id_course) references course(id_course) on delete set null
);


create table if not exists user_sport( -- Conexão dos usuarios com os esportes
						   id_user int ,
						   id_sport int not null ,
						   foreign key (id_user) references users(id_user),
						   foreign key (id_sport) references sport(id_sport) on delete cascade
);

create table if not exists user_number( -- Numero dos usuarios
							id_user int unique ,
							number_1 varchar(15),
							number_2 varchar(15),
							foreign key (id_user) references users(id_user) on delete cascade
);

create table if not exists user_admin( -- Tabela de quem é admin
						   id_user int unique,
						   password varchar(16) not null default '000000',
						   about text,
						   id_type int not null ,
						   foreign key (id_user) references users(id_user) ,
						   foreign key (id_type) references admin_type(id_type)
);

create table if not exists user_attempts_login( -- Registra as tentativas de login no sistema
									id_user int unique,
									attempts int default 0,
									user_blocked boolean default  0,
									blocked_date datetime default null,
									foreign key (id_user) references users(id_user) on delete cascade
);


create table if not exists log( -- Log de todas as ações do dashboard
					id_log bigint primary key auto_increment ,
					id_user int ,
					id_section int not null ,
					id_action int not null ,
					message varchar(255) not null ,
					date datetime not null default current_timestamp,
					foreign key (id_user) references users(id_user) on delete set null ,
					foreign key (id_section) references section(id_section),
					foreign key (id_action) references action(id_action)
);

create table if not exists alert( -- Message de alerta no site
					  id_alert int primary key auto_increment ,
					  id_user int,
					  priority int not null ,
					  title varchar(50),
					  text varchar(255) ,
					  path varchar(50) ,
					  expiration_date date ,
					  create_date datetime default current_timestamp,
					  foreign key (id_user) references users(id_user) on delete set null
);

create table if not exists visit_site( -- Log de acesso no site
    						id_visit int primary key auto_increment,
   							date_visit datetime default current_timestamp
);


create table if not exists form( -- forms da atletica
					 id_form int primary key auto_increment ,
					 title varchar(50) not null ,
					 sub_title varchar(50) ,
					 status boolean not null ,
					 date_limit datetime
);

create table if not exists form_answer_json( -- respostas permanentes dos formularios em json
								 id_form int not null ,
								 answer_json json ,
								 start_date_form datetime,
								 end_date_form datetime,
								 foreign key (id_form) references form(id_form)
);

create table if not exists form_question( -- perguntas dos formularios
							  id_question int primary key auto_increment ,
							  id_form int not null ,
							  question varchar(50) ,
							  foreign key (id_form) references form(id_form)
);

create table if not exists form_field( -- tipos de campos do formularios
						   id_field int primary key auto_increment ,
						   field varchar(20)
);

create table if not exists form_template( -- modelos dos formularios
							  id_form int not null ,
							  id_field int not null ,
							  id_question int not null ,
							  value varchar(255) not null ,
							  required boolean ,
							  foreign key (id_form) references form(id_form),
							  foreign key (id_field) references form_field(id_field),
							  foreign key (id_question) references form_question(id_question)
);

create table if not exists form_answer_temp( -- repostas temporarias dos formularios
								 id_form int not null ,
								 id_question int not null ,
								 answer varchar(255) ,
								 foreign key (id_form) references form(id_form),
								 foreign key (id_question) references form_question(id_question)
);