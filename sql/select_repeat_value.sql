
SELECT    class,
		  COUNT(class) AS Qtd
FROM  users
GROUP BY class
HAVING   COUNT(class) > 0
ORDER BY COUNT(class) DESC;


SELECT    date_visit,
		  COUNT(date_visit) AS Qtd
FROM  visit_site
GROUP BY date_visit
HAVING   COUNT(date_visit) > 0
ORDER BY COUNT(date_visit) DESC