<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Atlética Indomável</title>

		<link rel="stylesheet" href="css/index.css?<?php echo date("ymdHis"); ?>">
		<link rel="stylesheet" href="js/owlcarousel/owl.carousel.min.css">
		<link rel="stylesheet" href="js/owlcarousel/owl.theme.default.min.css">
		<link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="js/owlcarousel/owl.carousel.min.js"></script>
		<script src="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"></script>

		<script src="js/variables.js?<?php echo date("ymdHis"); ?>"></script>
		<script src="js/function_start.js?<?php echo date("ymdHis"); ?>"></script>
	</head>
	<body onresize="onresize()">
		<div class="bg"></div>
		<div class="content-info">
			<div class="alertBox">
				<h2>Avisos</h2>
				<i class="fas fa-times" onclick="closeMainAlert()"></i>
				<i class="fas fa-angle-right" onclick="next()"></i>
				<i class="fas fa-angle-left" onclick="prev()"></i>
				<div class="alertBox-content owl-carousel"></div>
			</div>
		</div>
		<div id="ma" class="main">
			<div id="menu-btn" class="fas fa-bars" onclick="menu()"></div>
			<div class="header">
				<div class="header-name" id="name"></div>
				<div class="header-menu">
					<a href="#S0">Home</a>
					<a href="#S1">Quem Somos</a>
					<a href="#S2">Galeria</a>
					<a href="#S3">Diretores</a>
					<a href="#S4">Parceria</a>
				</div>
			</div>
			<div class="content-main">
				<div id="top"></div>
				<div id="S0" class="session">
					<div class="content-home">
						<img src="img/logo.png" class="logo">
						<h1 id="title-index"></h1>
						<h3 id="sub-title-index"></h3>
					</div>
				</div>

				<div id="S1" class="session">
					<h2>Quem Somos</h2>
					<div class="content-about">
						<img src="img/logo.png" class="logo">
						<div class="flex-v center-v center-h">
							<p id="about"></p>
						</div>
					</div>
				</div>

				<div id="S2" class="session">
					<h2>Galeria</h2>
					<div class="content-gallery">
						<div class="gal owl-carousel"></div>
						<div class="gal owl-carousel"></div>
					</div>
				</div>

				<div id="S3" class="session">
					<h2>Diretores</h2>
					<div class="content-director"></div>
				</div>

				<div id="S4" class="session">
					<h2>Parcerias</h2>
					<div class="content-partner"></div>
				</div>

				<a class="toTop" href="#top"><i class="fas fa-caret-up"></i></a>
			</div>
		</div>
		<script src="js/index.js?<?php echo date("ymdHis"); ?>"></script>
	</body>
</html>