var content_info = $('.content-info');
var sel_type = $('select[name=type]');

$(document).ready(function () {
	sel_type.change(function () {
		getData('post',"type=" + $(this).find(':selected').val(),getUrlData(2),updateOffice);
	});

	getData('post',"type=" + sel_type.find(':selected').val(),getUrlData(2),updateOffice);
});

function updateOffice() {
	mountList(8);
}

function addOffice() {
	sendForm("formOfficeAdd", getUrlData(14), successSave);
}

function editOffice(index) {
	content_info.css('display','flex');

	let edit_office = $('#edit-office');

	edit_office.html(
		"<input type='hidden' name='id' value='" + (result_data[index].id) + "'>" +
		"<input type='hidden' name='type' value='" +  sel_type.find(':selected').val() + "'>" +
		"<input type='hidden' name='action' value='update'>" +
		"<li>Nome do curso: <input type='text' name='name' value='" + result_data[index].name + "'></li>"
	);
}


function saveOffice() {
	sendForm("formOfficeUpdate", getUrlData(14), successSave);
}

function successSave() {
	closeOffice();
	if(result_data.result === "ok"){
		getAllMenu(false);
		getData('post',"type=" + sel_type.find(':selected').val(),getUrlData(2),updateOffice);
		sendAlert("Cargo atualizado", "success");
	}else {
		sendAlert("Um erro ocorreu ao atualizar cargo", "danger");
	}
}


function deleteOffice(index) {
	getData("post","type=" +  sel_type.find(':selected').val() + "&action=delete&id=" + result_data[index].id, getUrlData(14), successSave);
}

function closeOffice() {
	content_info.css('display','none');
}