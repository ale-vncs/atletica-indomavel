
$(document).ready(function () {
	getData("post","type=getAlert", getUrlData(12), createAlertList);
	setDownRangeYear(0);

	for(let i = 0; i < 10; i++){
		$('select[name=priorityAlert]').append("<option>" + (i+1) + "</option>");
	}

	$('#input-img').change(function(){
		$("#imgAdmin").html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});
});

function createAlertList() {
	mountList(5);
}

function editAlert(index) {
	$(".content-info").eq(0).css('display','flex');

	let edit_alert = $('#edit-alert');
	edit_alert.html("");
	edit_alert.append("<input type='hidden' name='type' value='updateAlert'>" );
	edit_alert.append("<input type='hidden' name='id_alert' value='" + result_data[index].id_alert + "'>" );
	edit_alert.append("<input type='hidden' name='remImg' value='false'>");

	edit_alert.append("<input id='input-img-rem' name='imagem' type='file'><label class='btn info' for='input-img-rem' id='imgAdmin-rem'>"+ (result_data[index].path !== null ? "<img class='img' src='../img/alert/"+result_data[index].path+"'>" : "Selecione uma imagem") +"</label>");
	edit_alert.append("<li><div class='btn danger' onclick='removeImg()'>Remover Imagem</div></li>");
	edit_alert.append("<li>Titulo: <input type='text' name='titleAlert' value='" + result_data[index].title + "'></li>");
	edit_alert.append("<li>Mensagem: <input type='text' name='messageAlert' value='" + result_data[index].text + "'></li>");
	edit_alert.append("<li>Prioridade: <input type='text' name='priorityAlert' value='" + result_data[index].priority + "'></li>");
	edit_alert.append("<li>Data de Expiração: <input type='text' name='expirationDate' placeholder='99/99/9999' value='" + (result_data[index].expiration_date === '00/00/0000' ? '' : result_data[index].expiration_date) + "'></li>");

	$('#input-img-rem').change(function(){
		$("#imgAdmin-rem").html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});

	$('input[name=expirationDate]').mask('00/00/0000',{clearIfNotMatch: true})

}

function validation() {
	if($('input[name=titleAlert]').val() === "" && $('textarea[name=messageAlert]').val() === "" && $('#input-img').get(0).files.length === 0){
		sendAlert("Preencha os campos","danger");
		return false;
	}

	return true;
}

function saveAlert() {
	closeInfo();
	sendForm("formAlertUpdate", getUrlData(12), successAlertAdd ,($('#input-img').get(0).files.length !== 0));
}

function addAlert() {
	if(!validation()) return;
	sendForm("formAlertAdd", getUrlData(12), successAlertAdd, ($('#input-img').get(0).files.length !== 0));
}

function successAlertAdd() {
	sendAlert(result_data.msg, (result_data.result === "ok" ? "success":"danger" ));
	getData("post","type=getAlert",getUrlData(12), createAlertList);
	$("#imgAdmin").html("Selecione uma imagem");
	$("#input-img").val(null);
}

function closeInfo() {
	$(".content-info").eq(0).css('display','none');
}

function removeImg(){
	$('#input-img-rem').val("");
	$('input[name=remImg]').val("true");
	$("#imgAdmin-rem").html("Selecione uma imagem");
}

function deleteAlert(index) {
	getData("post","type=deleteAlert&id_alert="+result_data[index].id_alert,getUrlData(12),successDeleteAlert);
}

function successDeleteAlert() {
	sendAlert(result_data.msg, (result_data.result === "ok" ? "success":"danger" ));
	getData("post","type=getAlert", getUrlData(12), createAlertList, false);
}