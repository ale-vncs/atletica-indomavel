var content_info = $(".content-info");
var data_user = $("#data-user");
var edit_member = $("#edit-member");

$(document).ready(function () {
	let sel_sp = $("#sel_sp");
	sel_sp.html("<option value='0'>Todos</option>");
	for (let i = 0; i < menu_sport.length; i++){
		sel_sp.append("<option value='"+ menu_sport[i].id +"'>"+ menu_sport[i].name +"</option>");
	}

	let sel_cl = $("#sel_cl");
	sel_cl.html("<option value='0'>Todos</option>");
	for (let i = 0; i < menu_course.length; i++){
		sel_cl.append("<option value='"+ menu_course[i].id +"'>"+ menu_course[i].name +"</option>");
	}

	getData("post", "type=general", getUrlData(5), createTable);
	new SimpleBar(document.getElementsByClassName('more-info').item(0));
	new SimpleBar(document.getElementsByClassName('more-info').item(1));
});

function createTable() {
	mountList(1);
	hideLoading();
}


function sendFormMember() {
	sendForm("formMember", getUrlData(5), createTable);
}

function moreInfo(index) {
	content_info.eq(0).css('display','flex');
	data_user.html("");
	data_user.append("<li>Matricula: <p>" + result_data[index].registry + "</p></li>");
	data_user.append("<li>Nome: <p>" + result_data[index].name + "</p></li>");
	data_user.append("<li>Gênero: <p>" + result_data[index].gender + "</p></li>");
	data_user.append("<li>Email: <p>" + result_data[index].email + "</p></li>");
	data_user.append("<li>Telefone: <p>1: " + (result_data[index].number_1 === '' ? 'Sem numero' : result_data[index].number_1) + ", 2: " + (result_data[index].number_2 === '' ? 'Sem numero' : result_data[index].number_2) + "</p></li>");
	data_user.append("<li>Tipo: <p>" + result_data[index].type + "</p></li>");
	data_user.append("<li>Esporte: <p>" + result_data[index].sport + "</p></li>");
	data_user.append("<li>Curso: <p>" + result_data[index].course + "</p></li>");
	data_user.append("<li>Data de Aniversário: <p>" + result_data[index].birthday_date + "</p></li>");
	data_user.append("<li>Associado desde: <p>" + result_data[index].join_date + "</p></li>");
	data_user.append("<li>Data de associação: <p>" + result_data[index].association_date + "</p></li>");
	data_user.append("<li>Associado: <p>" + result_data[index].association + "</p></li>");
}

function editMember(index) {
	content_info.eq(1).css('display','flex');
	edit_member.html(
		"<input name='type' type='hidden' value='edit'>" +
		"<input name='id' type='hidden' value='" + result_data[index].id_user + "'>"
	);

	edit_member.append("<li><div class='checkboxes-and-radios'><input type='checkbox' name='enabled' id='en' " + (result_data[index].association === "Sim" ? "checked" : "") + "><label for='en'>Associado</label></div></li>");
	edit_member.append("<li>Matricula: <input name='registry' type='text' value='"+ result_data[index].registry +"'></li>");
	edit_member.append("<li>Nome: <input name='name' type='text' value='"+result_data[index].name+"'></li>");
	edit_member.append("<li>Email: <input name='email' type='text' value='"+result_data[index].email+"'></li>");
	edit_member.append("<li>Telefone 1: <input name='number_phone[]' type='text' value='"+result_data[index].number_1+"'></li>");
	edit_member.append("<li>Telefone 2: <input name='number_phone[]' type='text' value='"+result_data[index].number_2+"'></li>");
	edit_member.append("<li>Data de Aniversário: <input name='birthday_date' type='text' value='"+result_data[index].birthday_date+"'></li>");
	edit_member.append("<li>Gênero: <select class='sel_ge_ed' name='gender'></select></li>");
	edit_member.append("<li>Tipo: <select class='sel_ut_ed' name='user_type'></select></li>");
	edit_member.append("<li>Curso: <select class='sel_cl_ed' name='course'></select></li>");
	edit_member.append("<li>Esporte: <div class='checkboxes-and-radios sel_sp_ed'></div></li>");

	mountSelectMenu('gender',result_data[index].gender);
	mountSelectMenu('course',result_data[index].course);
	mountSelectMenu('user_type', result_data[index].type);
	mountSelectMenu('sport', result_data[index].sport);


	$("input[name='number_phone[]']").each(function(){
		$(this).mask('(00) 00000-0000', {clearIfNotMatch: true});
	});

	$('input[name=birthday_date]').mask('00/00/0000', {clearIfNotMatch: true});
}

function saveMember() {
	sendForm("formMemberUpdate", getUrlData(5), successSave);
}

function successSave() {
	if(result_data.result === "ok"){
		sendAlert("Membro atualizado");
		getData("post", "type=general", getUrlData(5), createTable);
		closeInfo();
	} else {
		sendAlert("Erro ao atualizar membro", "danger");
	}
}

function closeInfo() {
	content_info.css('display','none');
}