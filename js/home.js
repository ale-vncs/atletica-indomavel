var ch_date = document.getElementById("by_date");
var config_gp_gender, gp_gender;
var config_gp_courses, gp_courses;
var config_gp_sport, gp_sport;
var config_gp_visit, gp_visit;

var count_users, count_partner, count_alert;

ch_date.addEventListener("change",function () {
	getData("post",(ch_date.checked ? "byDate=true" : ""),getUrlData(13), generateData);
});

$(document).ready(function () {

	gp_gender = new Chart($('#graph_gender'), config_gp_gender);
	gp_courses = new Chart($('#graph_course'), config_gp_courses);
	gp_sport = new Chart($('#graph_sport'), config_gp_sport);
	gp_visit = new Chart($('#graph_visit'), config_gp_visit);

	count_users = new CountUp('countUsers', 0);
	count_partner = new CountUp('countPartner', 0);
	count_alert = new CountUp('countAlert', 0);

	getInfoHome();
	interval = setInterval(function () {
		getInfoHome();
	}, 10000);

});

function getInfoHome() {
	getData("post", "limit=4", getUrlData(1), createTable);
	getData("post",(ch_date.checked ? "byDate=true" : ""),getUrlData(13), generateData);
}

function createTable() {
	mountList(0);
}

function generateData() {

	let qt_course = [[],[]];
	let qt_gender = [[],[]];
	let qt_sport = [[],[]];
	let qt_visit = [[],[]];


	for(let i = 0; i < result_data['course'].length; i++){
		qt_course[0][i] = result_data['course'][i].name;
		qt_course[1][i] = result_data['course'][i].count;
	}

	for(let i = 0; i < result_data['gender'].length; i++){
		qt_gender[0][i] = result_data['gender'][i].gender;
		qt_gender[1][i] = result_data['gender'][i].count;
	}

	for(let i = 0; i < result_data['sport'].length; i++){
		qt_sport[0][i] = result_data['sport'][i].name;
		qt_sport[1][i] = result_data['sport'][i].count;
	}

	for(let i = 0; i < result_data['visit'].length; i++){
		qt_visit[0][i] = result_data['visit'][i].name;
		qt_visit[1][i] = result_data['visit'][i].count;
	}

	graphCourse(qt_course);
	graphGender(qt_gender);
	graphSport(qt_sport);
	graphVisit(qt_visit);

	animateNumber(result_data['count'][0]);
}

function animateNumber(data) {
	count_users.update(data.count_users);
	count_partner.update(data.count_partner);
	count_alert.update(data.count_alert);
}

function graphGender(data) {
	config_gp_gender.data.labels = data[0];
	config_gp_gender.data.datasets[0].data = data[1];
	gp_gender.update();
}


config_gp_gender = {
	type: 'pie',
	data: {
		labels: [],
		datasets: [{
			data: [],
			backgroundColor: graphBackgroundColor,
			borderColor: graphBorderColor,
			borderWidth: 1
		}]
	},
	options: {
		legend: {
			display: true,
			labels: {
				fontColor: graphFontColor,
				fontSize: graphFontSizeLegend
			},
			position: graphLegendPosition
		},
		title: {
			display: true,
			text: 'Generos',
			fontColor: graphFontColor,
			fontSize: graphFontSizeTitle
		}
	}
};

function graphCourse(data) {
	config_gp_courses.data.labels = data[0];
	config_gp_courses.data.datasets[0].data = data[1];
	gp_courses.update();
}

config_gp_courses = {
	type: 'horizontalBar',
	data: {
		labels: [],
		datasets: [{
			label: 'Quantidade',
			data: [],
			backgroundColor: graphBackgroundColor,
			borderColor: graphBorderColor,
			borderWidth: 1
		}]
	},
	options: {
		legend: {
			display: false,
			labels: {
				fontColor: graphFontColor,
				fontSize: graphFontSizeLegend
			}
		},
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero: true,
					fontColor: graphFontColor,
					fontSize: graphFontSizeLegend,
				},

			}],
			yAxes: [{
				ticks: {
					fontColor: graphFontColor,
					fontSize: graphFontSizeLegend,
				},
			}]
		},
		title: {
			display: true,
			text: 'Cursos',
			fontColor: graphFontColor,
			fontSize: graphFontSizeTitle
		}
	}
};

function graphSport(data) {
	config_gp_sport.data.labels = data[0];
	config_gp_sport.data.datasets[0].data = data[1];
	gp_sport.update();
}

config_gp_sport = {
	type: 'doughnut',
	data: {
		labels: [],
		datasets: [{
			data: [],
			backgroundColor: graphBackgroundColor,
			borderColor: graphBorderColor,
			borderWidth: 1
		}]
	},
	options: {
		legend: {
			display: true,
			labels: {
				fontColor: graphFontColor,
				fontSize: graphFontSizeLegend
			},
			 position: graphLegendPosition
		},
		title: {
			display: true,
			text: 'Esportes',
			fontColor: graphFontColor,
			fontSize: graphFontSizeTitle
		}
	}
};

function graphVisit(data) {
	config_gp_visit.data.labels = data[0];
	config_gp_visit.data.datasets[0].data = data[1];
	gp_visit.update();
}

config_gp_visit = {
	type: 'line',
	data: {
		labels: [],
		datasets: [{
			label: 'Quantidade',
			data: [],
			backgroundColor: graphBackgroundColor,
			borderColor: graphBorderColor,
			borderWidth: 1
		}]
	},
	options: {
		legend: {
			display: false,
			labels: {
				fontColor: graphFontColor,
				fontSize: graphFontSizeLegend
			}
		},
		title: {
			display: true,
			text: 'Acessos',
			fontColor: graphFontColor,
			fontSize: graphFontSizeTitle
		},
		scales: {
			xAxes: [{
				ticks: {
					fontColor: graphFontColor,
				},

			}],
			yAxes: [{
				stacked: true,
				ticks: {
					beginAtZero: true,
					fontColor: graphFontColor,
				},
			}]
		},
	}
};