var registry_account = $("input[name=registry]");
var name_account = $("input[name=name]");
var email_account = $("input[name=email]");
var birthday_date_account = $("input[name=birthday_date]");

var pass_new_account = $("input[name=password_new]");
var pass_current_account = $("input[name=password_current]");
var pass_new_again_account = $("input[name=password_new_again]");

var btn_account = $("input[name=btn]");
var remImg_account = $("input[name=remImg]");

var textarea = $("textarea").eq(0);

$(document).ready(function () {
	if(user_data.path !== null)
		$("#imgAdmin").html("<img class='img' src='../img/users/" + user_data.path + "'>");
	textarea.val(user_data.about);
	registry_account.val(user_data.registry);
	name_account.val(user_data.name);
	email_account.val(user_data.email);
	birthday_date_account.val(user_data.birthday_date);

	mountSelectMenu('gender', user_data.gender);
	mountSelectMenu('course', user_data.course);
	mountSelectMenu('sport', user_data.sport);

	birthday_date_account.mask('00/00/0000',{clearIfNotMatch: true});
	$('#input-img').change(function(){
		$('label').eq(0).html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});

	registry_account.mask('00000000',{clearIfNotMatch: true});

	hideLoading();
});

function validation() {
	if(pass_current_account.val().length < 5){
		sendPopup(pass_current_account,(pass_current_account.val().length  === 0 ? "Digite a senha para alterar!!!" : "Senha muito curta!!!"), "info");
		return  false;
	}

	if(pass_new_account.val() !== "" && pass_new_account.val().length < 5 ){
		sendPopup(pass_new_account,"Senha nova muito curta!!!", "danger", 'lc');
		return  false;
	}
	if(pass_new_account.val() !== pass_new_again_account.val()){
		sendAlert("Senhas não correspondem!!!", "danger");
		return false;
	}

	return true;
}

function setAccount() {
	if(!validation()) return;

	sendForm("formAccount",getUrlData(11), successAccount, ($('#input-img').get(0).files.length !== 0));
	btn_account.prop('disabled',true);
	btn_account.val("Aguarde...");
}

function successAccount() {
	if(result_data.result === "ok"){
		getData("post","", getUrlData(11), setStatus);
		menu();
		sendAlert(result_data.msg, "success");
	} else {
		sendAlert(result_data.msg, "danger");
	}
	btn_account.prop('disabled',false);
	btn_account.val("Salvar");
	remImg_account.val("false");
}

function removeImg(){
	$('#input-img').val("");
	remImg_account.val("true");
	$("#imgAdmin").html("<img class='margin-bottom img' src='../img/users/unknown_people.png'>");
}
