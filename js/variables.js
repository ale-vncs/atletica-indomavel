const min_width = 1000; //Variavel que define o tamanho minimo pra entrar no modo responsivel

let result_data = []; //Variavel que guarda a ultima resposta do banco de dados

let page_index = 0; //Variavel do Index da página que vai ser chamada quando entrar na dashboard

let interval = null; //Variavel que guarda um metodo que está sendo executado automaticamente

let user_data = []; //Variavel que guarda alguns dados do usuario

const urlData = [ //Variavel com o link de todos os arquivos de comunicação com o banco de dados
	"connection/get_set_about.php", //0
	"connection/get_log.php", //1
	"connection/get_menu.php", //2
	"connection/admin_login.php", //3
	"connection/get_set_gallery.php", //4
	"connection/get_set_member.php", //5
	"connection/get_set_director.php", //6
	"connection/get_set_sport.php", //7
	"connection/get_set_course.php", //8
	"connection/get_set_partner.php", //9
	"connection/check_session.php", //10
	"connection/get_set_account.php", //11
	"connection/get_set_alert.php", //12
	"connection/get_home_info.php", //13
	"connection/set_office.php", //14
];

const data_pages = [ //Array dos menus que seram criados quando usuario esta logado
	//Nome da pagina, nome do menu, css do icone, ativo, apenas presidente
	['home.php','Inicio','fas fa-home', true, false], //0
	['gallery.php','Galeria','fas fa-images', true, false], //1
	['director.php','Diretores','fas fa-user-shield', true, true], //2
	['member.php','Membros','fas fa-user-friends', true, false], //3
	['alert.php','Avisos','fas fa-exclamation-circle', true, false], //4
	['','Formulários','fas fa-align-center', false, false], //6
	['sport.php','Esportes','fas fa-volleyball-ball', true, true], //7
	['course.php','Cursos','fas fa-clipboard-list', true, true], //8
	['office.php','Cargos','fas fa-address-card', true, true], //9
	['partner.php','Parcerias','fas fa-handshake', true, false],//10
	['about.php','Sobre','fas fa-book', true, false], //11
	['account.php','Dados da Conta','fas fa-user-edit', true, false],//12
	['log.php','Log de Atividades','fas fa-book-open', true, false],//13
];

const sub_title = [ //Array de subtitulos apartir de um index
	//Nome do Subtitulo, nome depois do Index
	['Gerenciar', 'gallery.php'],
	['Conta', 'partner.php']
];

const graphColor = [ //rgb das cores do grafico
	'rgb(255, 99, 132)',
	'rgb(54, 162, 235)',
	'rgb(255, 206, 86)',
	'rgb(75, 192, 192)',
	'rgb(153, 102, 255)',
	'rgb(255, 159, 64)',
	'rgb(155, 205, 255)',
	'rgb(80, 255, 109)',
	'rgb(255, 13, 250)',
	'rgb(255, 1, 60)',
	'rgb(255, 237, 16)',
	'rgb(231, 186, 255)',
];



const graphBackgroundColor = [];
const graphBorderColor = [];
const graphFontColor = 'rgb(255, 255, 255)'; //Cor do texto do grafico
const graphFontSizeLegend = 11;
const graphFontSizeTitle = 14;
const graphLegendPosition = 'right';

for(let i = 0; i < graphColor.length; i++){
	let reg = /[\d+\s*,]*/g;
	graphBackgroundColor.push('rgba( ' + graphColor[i].match(reg)[4] + ',0.3)');
	graphBorderColor.push('rgba( ' + graphColor[i].match(reg)[4] + ',1)');
}

let menu_section = []; //Variavel com o menu da seção
let menu_action = []; //Variavel com o menu da ação
let menu_sport = []; //Variavel com o menu de esporte
let menu_course = []; //Variavel com o menu do curso
let menu_user_type = []; //Variavel com o menu do tipo de usuario
let menu_admin_type = []; //Variavel com o menu do tipo de admin/diretor
let menu_gender = []; //Variavel com o menu do tipo de genero
let menu_member = []; //Variavel com os nomes dos membros

