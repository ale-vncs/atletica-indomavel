getData("post", "", getUrlData(10), showBody);
showLoading($('#ma'));
const header = $(".header").eq(0);
const btn_menu = $("#menu-btn");
const men = header.find('a');
const session = $(".session");
const owl = $(".alertBox-content");

$(document).ready(function() {

	setInterval(function() {
		onscroll();
	}, 100);

	getData("post", "", getUrlData(0), setAbout);
	getData("post", "type=getGallery", getUrlData(4), showImg);
	getData("post", "type=getDirector", getUrlData(6), mountDirector);
	getData("post", "type=getPartner", getUrlData(9), mountPartner);
	getData("post", "type=getAlert", getUrlData(12), mountAlert);
	getData("post", "", "connection/set_visit.php");

	setBgColor();
	setSessionHeight();

	new SimpleBar(document.getElementsByClassName('content-main').item(0));
});

function onresize() {
	if (window.innerWidth > min_width) {
		header.css('right', 0);
		btn_menu.css('right', header.outerWidth() + "px");
		btn_menu.removeClass().addClass("fas fa-times");
	} else {
		header.css('right', -header.outerWidth() + "px");
		btn_menu.css('right', 0);
		btn_menu.removeClass().addClass("fas fa-bars");
	}
	setSessionHeight();
}

function setAbout() {
	$("#about").html(result_data.about_text);
	$("#title-index").html(result_data.title_index);
	$("#sub-title-index").html(result_data.sub_title_index);
	$("#name").html(result_data.name);
	if (result_data.enabled_association === "1")
		document.getElementsByClassName("content-home").item(0).innerHTML += "<a class='btn success' href='association.html'>Associar-se</a>";
}

function onscroll() {
	/*Efeito de Menu*/
	for (let i = 0; i < men.length; i++) {
		if (isScrolledIntoView('#S' + i)) {
			men.removeClass();
			men.eq(i).addClass("active");
		} else {
			men.eq(i).removeClass();
		}
	}

	if (!isScrolledIntoView('#top')) {
		$(".toTop").eq(0).css('bottom', '40px');
		if (window.innerWidth > min_width)
			header.css('boxShadow', '0px 5px 5px rgba(0,0,0,0.3)');
	} else {
		$(".toTop").eq(0).css('bottom', '-150px');
		if (window.innerWidth > min_width)
			header.css('boxShadow', 'none');
	}
}

function menu() {
	if (header.css('right') === "0px") {
		header.css('right', -header.outerWidth() + "px");
		btn_menu.css('right', 0);
		btn_menu.removeClass().addClass("fas fa-bars");
	} else {
		header.css('right', 0);
		btn_menu.css('right', header.outerWidth() + "px");
		btn_menu.removeClass().addClass("fas fa-times");
	}
}

function showImg() {
	let gallery = $(".gal");
	if (result_data.result === "error") {
		gallery.eq(0).parent().html("<p>Nenhuma imagem na galeria</p>");
		return;
	} else {
		gallery.eq(0).html("");
		gallery.eq(1).html("");
	}

	for (let i = 0; i < result_data.length; i++) {
		gallery.eq(i % 2 === 0 ? 0 : 1).append("<img src='img/gallery/" + result_data[i].path + "'>");
	}
	gallery.owlCarousel({
		loop: true,
		margin: 10,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 2,
			},
			1000: {
				items: 3,
			},
			1500: {
				items: 4,
			}
		}
	});
}
function mountAlert() {
	hideLoading();
	if (result_data.result === "error")
		return;
	$(".content-info").eq(0).css('display', 'flex');
	let alertBox = owl.eq(0);
	alertBox.html("");
	for (let i = 0; i < result_data.length; i++) {
		alertBox.append(
			"<div class='alertBox-item'>" +
				(result_data[i].path !== null ? "<img src='img/alert/" + result_data[i].path + "'>" : "") +
				(result_data[i].title !== null ? "<h3>" + result_data[i].title + "</h3>" : "") +
				(result_data[i].text !== null ? "<p>" + result_data[i].text + "</p>" : "") +
				(result_data[i].expiration_date !== "00/00/0000" ? "<h4>" + result_data[i].expiration_date + "</h4>" : "") +
			"</div>"
		);
	}

	owl.owlCarousel({
		loop: true,
		margin: 10,
		items: 1,
		autoplay: false,
	});
}

function closeMainAlert() {
	$(".content-info").eq(0).css('display', 'none');
}

function mountDirector() {
	let director = $(".content-director").eq(0);
	director.html("");
	for (let i = 0; i < result_data.length; i++) {
		director.append(
			"<div class='director-card'>" +
				"<img src='img/users/" + result_data[i].path + "'>" +
				"<h3>" + result_data[i].name + "</h3>" +
				"<p>" + result_data[i].about +
				"</p>" +
			"</div>"
		);
	}
}

function mountPartner() {
	let partner = $(".content-partner").eq(0);
	partner.html();
	for (let i = 0; i < result_data.length; i++) {
		partner.append(
			"<div class='partner-card'>" +
				"<img src='img/partner/" + result_data[i].path + "'>" +
				"<h3>" + result_data[i].name + "</h3>" +
				"<p>" + result_data[i].about + "</p>" +
			"</div>"
		);
	}
}

function setSessionHeight() {
	for (let i = 0; i < session.length; i++) {
		session.eq(i).css('height', window.innerHeight - (window.innerWidth > min_width ? header.outerHeight() : 0) + "px");
	}
}

function next() {
	owl.trigger('next.owl');
}

function prev() {
	owl.trigger('prev.owl');
}
