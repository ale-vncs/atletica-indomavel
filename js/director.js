var content_info = $(".content-info");


$(document).ready(function () {
	getData("post", "type=getDirector", getUrlData(6), startDirector);
});

function startDirector() {

	mountSelectMenu('member_list');
	mountSelectMenu('admin_type');
	mountList(2);
}

function showDirectorAdd() {
	content_info.eq(0).css('display',"flex");
}

function addDirector() {
	sendForm("formDirectorAdd", getUrlData(6), successDirectorAdd);
}

function successDirectorAdd() {
	if(result_data.result === "ok"){
		sendAlert(result_data.msg, "success");
		getData("post", "type=getDirector", getUrlData(6), startDirector);
	} else {
		sendAlert(result_data.msg, "danger");
	}
	closeInfo(0);
}

function editDirector(index) {
	content_info.eq(1).css('display',"flex");

	$("#edit-director").html(
		"<input name='type' type='hidden' value='updateDirector'>" +
		"<input name='id_user' type='hidden' value='" + result_data[index].id_user + "'>" +
		"<li>Tipo: <select name='id_type' class='sel_at_ed'></select></li>"
	);

	mountSelectMenu('admin_type',result_data[index].type);
}

function deleteDirector(index) {
	getData("post","type=delDirector&id_user="+result_data[index].id_user, getUrlData(6), successDirectorRem);
}

function successDirectorRem() {
	if(result_data.result === "ok"){
		sendAlert(result_data.msg, "success");
		getData("post", "type=getDirector", getUrlData(6), startDirector);
	} else {
		sendAlert(result_data.msg, "danger");
	}
	closeInfo(2);
}

function saveDirector() {
	sendForm("formDirectorUpdate" ,getUrlData(6),successDirector);
}

function successDirector() {
	if(result_data.result === "ok"){
		sendAlert("Diretor atualizado", "success");
		getData("post", "type=getDirector", getUrlData(6), startDirector);
	}
	closeInfo(1);
}

function closeInfo(index) {
	content_info.eq(index).css('display',"none");
}