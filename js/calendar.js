
var mouth =[
	['Janeiro', 31],
	['Fevereiro', 28],
	['Março', 31],
	['Abril', 30],
	['Maio', 31],
	['Junho', 30],
	['Julho', 31],
	['Agosto', 31],
	['Setembro', 30],
	['Outubro', 31],
	['Novembro', 30],
	['Dezembro', 31]
];

var cur_year = new Date().getFullYear();
var down_range_year = 10;
var up_range_year = 10;

var cal = document.getElementById("calendar");

cal.innerHTML += "<input type='hidden' id='date' name='date'>";
cal.innerHTML += "<select id='day' name='day' onchange='onSetDay()'></select>";
cal.innerHTML += "<select id='mouth' name='mouth' onchange='onChange()'></select>";
cal.innerHTML += "<select id='year' name='year' onchange='onChange()'></select>";

var sel = cal.getElementsByTagName("select");

$(document).ready(function () {
	setMouth();
	setYear();
	onChange();
});

function setMouth(){
	sel.item(1).innerHTML = "<option value='0'>Selecione um mês</option>";
	for(let i = 0; i < mouth.length; i++){
		if (i < 10){
			sel.item(1).innerHTML += "<option value='0" + (i+1) + "'>" + mouth[i][0] + "</option>";
		} else {
			sel.item(1).innerHTML += "<option value='" + (i+1) + "'>" + mouth[i][0] + "</option>";
		}
	}
}

function setYear(){
	sel.item(2).innerHTML = "<option value='0'>Selecione um ano</option>";
	for(let i = cur_year-down_range_year; i <= cur_year+up_range_year; i++){
		sel.item(2).innerHTML += "<option value='" + i + "'>" + i + "</option>";
	}
}

function onChange() {
	sel.item(0).innerHTML = "<option value='0'>Selecione um dia</option>";
	if(sel.item(1).value === '0') return;
	for (let i = 1; i <= mouth[sel.item(1).value-1][1]; i++){
		if (i < 10){
			sel.item(0).innerHTML += "<option value='0" + i + "'>0" + i + " </option>";
		} else {
			sel.item(0).innerHTML += "<option value='" + i + "'>" + i + " </option>";
		}
		if ((sel.item(1).value === '02') && (i+1 >= mouth[sel.item(1).value-1][1])){
			if (sel.item(2).value%4 === 0){
				sel.item(0).innerHTML += "<option value='" + (i+1) + "'>" + (i+1) + " </option>";
			}
		}
	}
}

function onSetDay() {
	document.getElementById("date").value = sel.item(0).value + "/" + sel.item(1).value + "/" + sel.item(2).value;
}

function getDateCalender() {
	return sel.item(0).value + "/" + sel.item(1).value + "/" + sel.item(2).value;
}

function setDownRangeYear(new_range) {
	down_range_year = new_range;
	setYear();
}

function setUpRangeYear(new_range) {
	up_range_year = new_range;
	setYear();
}

function resetCalendar() {
	up_range_year = down_range_year = 10;
	setYear();
	setMouth();
	onChange();
}