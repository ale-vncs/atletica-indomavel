check();

let btn_menu = $("#menu-btn");
let side_bar = $(".side-bar").eq(0);
let side_bar_header = $(".side-bar-header").eq(0);
const side_bar_width = side_bar.width();


$(document).ready(function () {
	getAllMenu(true);
	getData("post","",getUrlData(11), mountMenu); //Recebe os status do usuario
	setBgColor();
	onresize();
	new SimpleBar(document.getElementsByClassName('side-bar-body').item(0));
	new SimpleBar(document.getElementsByClassName('page').item(0));
});

function menu(index = page_index){ //Metodo para a mudança de pagina no dashboard
	page_index = index; //Seta a pagina atual
	clearInterval(interval); //Limpar a requisões automaticas
	showLoading($('.page').eq(0));//Mostra tela de loading
	$(".side-bar-item").removeClass("active");
	document.getElementsByClassName(data_pages[index][2])[0].parentElement.classList.add("active"); //Atribui a classe de ativo ao menu selecionado
	$("#page").load("pages/" + data_pages[index][0]);//carrega a pagina dentro da div page
	$("#name-page").text(data_pages[index][1]); //Seta o nome no titulo da page
	if(window.innerWidth < min_width){
		hideSideBar();
	}
}

function onresize() { //Metodo chamado quando é redimensionado a tela
	if (window.innerWidth < min_width){
		hideSideBar();
	}else{
		showSideBar();
	}
}

function menu_side() { //Ativa/Desativa o menu lateral
	if (side_bar.css('width') !== "0px"){
		hideSideBar();
	}else{
		showSideBar();
	}
}

function hideSideBar() {
	btn_menu.removeClass().addClass("fas fa-bars");
	side_bar.css('width','0px');
}

function showSideBar() {
	btn_menu.removeClass().addClass("fas fa-times");
	side_bar.css('width',side_bar_width);
}

function setStatus() { //Seta os status do admin e monta o menu lateral com os menus apropriados
	user_data = result_data;

	let name_user = user_data.name.split(' ');
	side_bar_header.find("h3").eq(0).html(name_user[0] + " " + name_user[name_user.length-1]);
	side_bar_header.find("h4").eq(0).html(user_data.status);
	side_bar_header.find("img").eq(0).prop('src',"../img/users/" + user_data.path);
}

function mountMenu() {
	setStatus();
	let side_bar_body = $('.side-bar-menu').eq(0);
	for(let i = 0; i < data_pages.length; i++){ //Seleciona os menus mostrados para o presidente ou usuarios
		if (data_pages[i][3]){
			if(data_pages[i][4] && user_data.high){
				side_bar_body.append("<div class='side-bar-item' onclick='menu("+ i +")'><i class='"+ data_pages[i][2] +"'></i><a>"+ data_pages[i][1] +"</a></div>");
			} else if(!data_pages[i][4]){
				side_bar_body.append("<div class='side-bar-item' onclick='menu("+ i +")'><i class='"+ data_pages[i][2] +"'></i><a>"+ data_pages[i][1] +"</a></div>");
			}
		}
		for (let j = 0; j < sub_title.length; j++){
			if (data_pages[i][0] === sub_title[j][1]){
				side_bar_body.append("<p>"+ sub_title[j][0] +"</p>");
			}
		}
	}
	//side_bar_body.parent().append("<div style='flex: 1; width: 100%; height: 100%; position: relative; background: var(--color-opacity-black);'></div>");

	menu(page_index); //Chama a pagina atual
}
