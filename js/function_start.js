function isScrolledIntoView(elem, win = $(window)){ //Verifica se uma div esta sendo mostrado pelo usuario
	let docViewTop = win.scrollTop();
	let docViewBottom = docViewTop + win.height();

	let elemTop = $(elem).offset().top+400;
	let elemBottom = elemTop + $(elem).height()-300;
	return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

function mountList(columnIndex) { //Monta uma tabela especifica
	if (result_data !== null && result_data.result === "error"){
		sendAlert("Nenhuma valor encontrado!!!");
	}

	let listContent = $(".list").eq(0);
	listContent.html("");
	if (result_data !== null){
		for (let i = 0; i < result_data.length; i++) {
			if (columnIndex === 0){ //List Log
				listContent.append(
					"<div class='list-card'>" +
						"<h4>" + result_data[i].section +  "</h4>" +
						"<h4>" + result_data[i].action +  "</h4>" +
						"<h4>" + result_data[i].name +  "</h4>" +
						"<p>" + result_data[i].message + "</p>" +
						"<p>" + result_data[i].date + "</p>" +
					"</div>"
				);
			} else if (columnIndex === 1){ //List Member
				listContent.append(
					"<div class='list-card'>" +
						"<img src='../img/users/" + result_data[i].path + "'>" +
						"<h4>" + result_data[i].name +  "</h4>" +
						"<p>" + result_data[i].type + "</p>" +
						"<p>" + result_data[i].course + "</p>" +
						"<div class='btn info' onclick='moreInfo(" + i + ")'>Mais info</div>" +
						"<div class='btn warning' onclick='editMember(" + i + ")'>Editar Membro</div>" +
					"</div>"
				);
			} else if (columnIndex === 2){ //List Director
				listContent.append(
					"<div class='list-card'>" +
						"<img src='../img/users/" + result_data[i].path + "'>" +
						"<h4>" + result_data[i].name +  "</h4>" +
						"<p>" + result_data[i].about + "</p>" +
						"<p>" + result_data[i].type + "</p>" +
						"<div class='btn success' onclick='editDirector(" + i + ")'>Editar</div>" +
						"<div class='btn danger' onclick='showConfirm(deleteDirector, \"Deseja remover o " + result_data[i].name +  " da direção?\",\"" + i + "\",\"../img/users/" + result_data[i].path + "\")'>Remover</div>" +
					"</div>"
				);
			} else if (columnIndex === 3){ //List Sport
				listContent.append(
					"<div class='list-card'>" +
						"<h4>" + result_data[i].name +  "</h4>" +
						"<div class='btn success' onclick='editSport(" + i + ")'>Editar</div>" +
						"<div class='btn danger' onclick='showConfirm(deleteSport, \"Deseja deletar o esporte " + result_data[i].name +  "?\",\"" + i + "\")'>Remover</div>" +
					"</div>"
				);
			} else if (columnIndex === 4){ //List Course
				listContent.append(
					"<div class='list-card'>" +
						"<h4>" + result_data[i].name +  "</h4>" +
						"<div class='btn success' onclick='editCourse(" + i + ")'>Editar</div>" +
						"<div class='btn danger' onclick='showConfirm(deleteCourse, \"Deseja deletar o curso de " + result_data[i].name +  "?\",\"" + i + "\")'>Remover</div>" +
					"</div>"
				);
			} else if (columnIndex === 5){ //List Alert
				listContent.append(
					"<div class='list-card'>" +
						(result_data[i].path !== null ? "<img src='../img/alert/"+result_data[i].path+"'>" : "<p>Sem imagem</p>") +
						"<h4>" + (result_data[i].title === "" ? "Sem titulo" : result_data[i].title) +  "</h4>" +
						"<p>" + (result_data[i].text === "" ? "Sem observação" : result_data[i].text) + "</p>" +
						"<p>" + (result_data[i].expiration_date === "00/00/0000" ? "Sem limite" : result_data[i].expiration_date) + "</p>" +
						"<div class='btn warning' onclick='editAlert(" + i + ")'>Editar</div>" +
						"<div class='btn danger' onclick='showConfirm(deleteAlert,\"Deseja remover esse aviso?\", \""+ i +"\",\"../img/alert/"+ result_data[i].path +"\")'>Remover</div>" +
					"</div>"
				);
			} else if(columnIndex === 6){ //List Gallery
				listContent.append(
					"<div class='list-card'>" +
						"<img src='../img/gallery/"+ result_data[i].path +"'>" +
						"<div class='btn danger' onclick='showConfirm(deleteGallery,\"Deseja remover essa imagem?\", \""+ i +"\",\"../img/gallery/"+ result_data[i].path +"\")'>Remover</div>" +
					"</div>"
				);
			} else if(columnIndex === 7){ //List Partner
				listContent.append(
					"<div class='list-card'>" +
						"<img src='../img/partner/"+ result_data[i].path +"'>" +
						"<p>" + result_data[i].name + "</p>" +
						"<div class='btn warning' onclick='editPartner("+ i +")'>Editar</div>" +
						"<div class='btn danger' onclick='showConfirm(deletePartner,\"Deseja remover esse parceiro?\", \""+ i +"\",\"../img/partner/"+ result_data[i].path +"\")'>Remover</div>" +
					"</div>"
				);
			} else if(columnIndex === 8){
				listContent.append(
					"<div class='list-card'>" +
						"<p>" + result_data[i].name + "</p>" +
					"<div class='btn warning' onclick='editOffice("+ i +")'>Editar</div>" +
					"<div class='btn danger' onclick='showConfirm(deleteOffice,\"Deseja remover esse cargo?\", \""+ i +"\")'>Remover</div>" +
					"</div>"
				);
			}
		}
	}
	hideLoading();
}

function getData(method, parameter, url, func = null, async = true) { //Metodo para recuperar dados do banco de dados
	$.ajax({
		url: url,
		type: method,
		async: async,
		dataType: 'html',
		data: parameter
	}).done(function (data) {
		data = data.trim();
		if (data.length === 0) {
			data = "{}";
		}
		if(window.location.href.match('localhost:8085')) console.log(url + " -> " + data);
		try{
			result_data = JSON.parse(data);
		} catch (e) {
			result_data.result = "jsonError";
			sendAlert("Erro ao obter json","danger");
		}
	}).always(function() {
		if(result_data.page === undefined){
			if(func != null) func();
		} else {
			window.location.href = result_data.page;
		}

	});
}

function sendForm(formId, url, func, barEnabled = false){// Envia formulario
	let formFiles = document.getElementById(formId);
	let progressBar, bar;
	if(barEnabled){
		progressBar = $("#progressBar");
		progressBar.html("<p></p>");
		bar = progressBar.find("p").eq(0);
		bar.css('width','0px');
	}


	$.ajax({
		url: url,
		type: 'post',
		//dataType: 'html',
		data:  new FormData(formFiles),
		error: function(){
			console.log('error sendForm');
		},
		success: function(data) {
			data = data.trim();
			if(window.location.href.match('localhost:8085')) console.log(data);
			try{
				result_data = JSON.parse(data);
			} catch (e) {
				result_data.result = "jsonError";
				sendAlert("Algum erro ocorreu","danger");
			}
			if(barEnabled){
				setTimeout(function () {
					progressBar.css('height','0px');
				}, 4000);
			}
			func();
		},
		cache: false,
		contentType: false,
		processData: false,
		xhr: function() { // Custom XMLHttpRequest

			var myXhr = $.ajaxSettings.xhr();
			if(barEnabled){
				if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
					myXhr.upload.addEventListener('progress', function(evt) {
						let pct = Math.floor((evt.loaded * 100) / evt.total);
						let num = progressBar.width() * pct / 100;
						progressBar.css('height', bar.outerHeight());
						bar.css('width', num + "px").text(pct + "%");
					}, false);
				}
			}
			return myXhr;
		}
	});
}

function sendAlert(msg, model = "success") { //Metodo para mandar alguma mensagem de alerta no dashboard

	let box = $("<li class='" + model + "'><p>" + msg + "</p></li>");
	$("#alert").append(box);
	box.height(box.outerHeight(true));
	setTimeout(function() {
		box.css('height','0');
		setTimeout(function () {
			box.remove();
		},800);
	}, 5000);

	box.click(function () {
		$(this).css('height','0');
	});
}

function openTab() { //metodo de manipulação das Tab, não utilizado.....
	let tab_box = $('.tab-box');
	let tab_button = $(".tab-button");

	$(".tab-container-button").append("<div class='tab-button-effect'></div>");;

	let tab_effect = $('.tab-button-effect');

	tab_effect
		.width(tab_button.outerWidth(true))
		.height(tab_button.outerHeight(true))
		.css('top',tab_button.eq(0).position().top+"px")
		.css('left',tab_button.eq(0).position().left+"px");

	tab_button.eq(0).addClass('active');

	tab_box.css('height','0');
	tab_box.eq(0).css('height',tab_box.eq(0).find('section').outerHeight(true));

	tab_button.click(function () {
		tab_button.removeClass('active');
		$(this).addClass('active');
		tab_box.css('height','0');
		tab_box.eq($(this).index()).css('height',tab_box.eq($(this).index()).find('section').outerHeight(true));
	});

	interval = setInterval(function () {
		let btn = $('.tab-button.active');
		tab_effect
			.width(btn.outerWidth(true))
			.height(btn.outerHeight(true))
			.css('top',btn.position().top+"px")
			.css('left',btn.position().left+"px")
	}, 100);
}

function showLoading(elem, msg = 'Carregando...') {
	if($('#loading-page').is(":visible")) return;
	let size = "height: " + elem.outerHeight() + "px; width: " + elem.outerWidth() + "px; top: " + elem.offset().top + "px; left: " + elem.offset().left + "px;";
	$(document.body).append(
		"<div id='loading-page' style='" + size + "'>" +
			"<div>" +
				"<img alt='' src='" + (window.location.href.match('admin') ? '../img/loading.png' : 'img/loading.png') + "'>" +
				"<h2>" + msg + "</h2>" +
			"</div>" +
		"</div>"
	);
}

function hideLoading() { //Metodo para esconder a tela de loading
	$("#loading-page").remove();
}

function getAllMenu(async = true){
	getData("post", "type=section", getUrlData(2), function () {
		menu_section = result_data;
	}, async);
	getData("post", "type=action", getUrlData(2), function () {
		menu_action = result_data;
	}, async);
	getData("post", "type=sport", getUrlData(2), function () {
		menu_sport = result_data;
	}, async);
	getData("post", "type=course", getUrlData(2), function () {
		menu_course = result_data;
	}, async);
	getData("post", "type=user_type", getUrlData(2), function () {
		menu_user_type = result_data;
	}, async);
	getData("post", "type=admin_type", getUrlData(2), function () {
		menu_admin_type = result_data;
	}, async);
	getData("post", "type=gender", getUrlData(2), function () {
		menu_gender = result_data;
	}, async);
	getData("post", "type=member", getUrlData(2), function () {
		menu_member = result_data;
	}, async);
}

function mountSelectMenu(name, array = null, valueDefault = 'Selecione...') {
	if(name === 'course'){
		let sel_cl_ed = $(".sel_cl_ed");
		sel_cl_ed.html("<option value='null'>" + valueDefault + "</option>");
		for (let i = 0; i < menu_course.length; i++){
			sel_cl_ed.append( "<option value='"+ menu_course[i].id +"' " + (menu_course[i].name === array ? 'selected' : '') + ">"+ menu_course[i].name +"</option>");
		}
	} else if(name === 'gender'){
		let sel_ge_ed = $(".sel_ge_ed");
		sel_ge_ed.html("<option value='null'>" + valueDefault + "</option>");
		for (let i = 0; i < menu_gender.length; i++){
			sel_ge_ed.append("<option value='"+ menu_gender[i].id +"' "+ (menu_gender[i].name === array ? 'selected' : '') +">"+ menu_gender[i].name +"</option>");
		}
	} else if(name === 'user_type'){
		let sel_ut_ed = $(".sel_ut_ed");
		sel_ut_ed.html("<option value='null'>" + valueDefault + "</option>");
		for (let i = 0; i < menu_user_type.length; i++){
			sel_ut_ed.append("<option value='"+ menu_user_type[i].id +"' " + (menu_user_type[i].name === array ? 'selected' : '') + ">"+ menu_user_type[i].name +"</option>");
		}
	} else if(name === 'admin_type'){
		let sel_ut_ed = $(".sel_at_ed");
		sel_ut_ed.html("<option value='null'>" + valueDefault + "</option>");
		for (let i = 0; i < menu_admin_type.length; i++){
			sel_ut_ed.append("<option value='"+ menu_admin_type[i].id +"' " + (menu_admin_type[i].name === array ? 'selected' : '') + ">"+ menu_admin_type[i].name +"</option>");
		}
	} else if(name === 'member_list'){
		let sel_cl_ed = $(".sel_mb_ed");
		sel_cl_ed.html("<option value='null'>" + valueDefault + "</option>");
		for (let i = 0; i < menu_member.length; i++){
			sel_cl_ed.append("<option value='"+ menu_member[i].id_user +"'>"+ menu_member[i].name +"</option>");
		}
	} else if(name === 'sport'){
		let sel_sp_ed = $(".sel_sp_ed");
		for (let i = 0; i < menu_sport.length; i++){
			sel_sp_ed.append("<input type='checkbox' name='sport[]' value="+menu_sport[i].id+" id=sport_"+i+ (array!==null && (array).split(",").includes(menu_sport[i].name) ? " checked" : "" )+" ><label for=sport_" + i +">"+menu_sport[i].name+"</label>");
		}
	}
}

function sendPopup(component, msg, mode = 'success', position = 'rc') {
	let size_popup_height = 15;
	let size_popup_width = msg.length*8 + 45;
	let comp_height = component.outerHeight(true);
	let comp_width = component.outerWidth(true);
	let pos_comp = component.offset();

	let border, arrow_pos, pos;

	//border  = "var(--color-" + mode + ") transparent transparent transparent;"; //baixo
	//border  = "transparent var(--color-" + mode + ") transparent transparent;"; //esquerda
	//border  = "transparent transparent var(--color-" + mode + ") transparent;"; //cima
	//border  = "transparent transparent transparent var(--color-" + mode + ");"; //direita

	if(position === 'rc'){
		pos = "left: " + (pos_comp.left + comp_width) + "px; top: " + (pos_comp.top + comp_height/2 - size_popup_height) + "px;";
		arrow_pos = "left: 0%; top: 50%; margin-left: -10px; margin-top: -5px;";
		border  = "transparent var(--color-" + mode + ") transparent transparent;";
	} else if(position === 'lc'){
		pos = "left: " + (pos_comp.left - size_popup_width) + "px; top: " + (pos_comp.top + comp_height/2 - size_popup_height) + "px;";
		arrow_pos = "left: 100%; top: 50%; margin-top: -5px;";
		border  = "transparent transparent transparent var(--color-" + mode + ");";
	} else if(position === 'tc'){
		pos = "left: " + (pos_comp.left + comp_width/2 - size_popup_width) + "px; top: " + (pos_comp.top - size_popup_height*2) + "px;";
		arrow_pos = "left: 50%; top: 100%; margin-left: -5px;";
		border  = "var(--color-" + mode + ") transparent transparent transparent;";
	} else if(position === 'bc'){
		pos = "left: " + (pos_comp.left + comp_width/2 - size_popup_width) + "px; top: " + (pos_comp.top + comp_height + size_popup_height*2) + "px;";
		arrow_pos = "left: 50%; top: 0%; margin-left: -5px; margin-top: -10px;";
		border  = "transparent transparent var(--color-" + mode + ") transparent;";
	}

	$(component.parent()).append(
		"<div class='popup " + mode + "' style='" + pos + "' onclick='$(this).remove();'>" +
			"<span style='border-color: " + border + "; " + arrow_pos + "'></span>" +
			"<p>" + msg + "</p>" +
		"</div>"
	);
}

function showConfirm(func, msg, param = null, imgUrl = null,) {
	$('.page').append(
		"<div id='confirm' class='content-info'>" +
			"<div class='more-info'>" +
				(imgUrl !== null ? "<img src='" + imgUrl + "' />" : "") +
				"<p>" + msg + "</p>" +
				"<div>" +
					"<div class='btn success'>Sim</div>" +
					"<div class='btn danger' onclick='$(\"#confirm\").remove()'>Não</div>" +
				"</div>" +
			"</div>" +
		"</div>"
	);
	$('#confirm').css('display','flex');
	$('#confirm .more-info div .btn.success').click(function () {
		func(param);
		$("#confirm").remove();
	});
}

function setBgColor() {
	getData("post","",getUrlData(0), function () {
		document.documentElement.style.setProperty('--color-primary', result_data.color_primary);
		document.documentElement.style.setProperty('--color-secondary', result_data.color_secondary);
		document.documentElement.style.setProperty('--color-text', result_data.color_text);
	});
}

function check(){ //Metodo para checar o tipo de acesso para evitar invasão
	getData("post","",getUrlData(10), showBody);
}

function showBody() {
	$(document.body).show();
}

function testScript() {
	if(!window.location.href.match('localhost:8085')) return;
	//sendAlert( 'Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo Metodo', 'danger');
	//sendPopup($('#user_img'), 'Metodo', 'danger');
	showLoading($('.page'));
}

function getUrlData(index) {
	return window.location.href.match('admin') ? "../" + urlData[index] : urlData[index];
}
