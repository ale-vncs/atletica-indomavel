check();
let field_login = $("#login");
let field_password = $("#password");
let btnLogin = $("#btnLogin");

$(document).ready(function () {
	$('#formLogin').keydown(function (event) {
		if(event.which === 13 ){
			onLogin();
		}
	});

	setBgColor();
});

function validation() {
	let pass = true;
	if (field_login.val() === ""){
		sendPopup(field_login,'A matricula é obrigatória!!!', 'danger');
		pass = false;
	}else if(field_login.val().match("\w+")){
		sendPopup(field_login,"A matricula é somente número!!!", "danger");
		pass = false;
	}

	if (field_password.val() === ""){
		sendPopup(field_password,"A senha é obrigatória!!!", "warning");
		pass = false;
	} else if (field_password.val().length < 6){
		sendPopup(field_password,"Senha muito curta...", "warning");
		pass = false;
	}

	return pass;
}

function onLogin() {
	if (!validation()){
		return;
	}
	btnLogin.val("Aguarde...");
	btnLogin.prop('disabled',true);
	sendForm("formLogin", getUrlData(3), isLogin);
}

function isLogin() {
	if (result_data.result === "ok"){
		window.location.href = "dashboard.php";
	} else if (result_data.result === "error"){
		sendAlert(result_data.msg , "danger");
		btnLogin.val("Acessar");
		btnLogin.prop('disabled',false);
	} else {
		sendAlert("Um erro desconhecido ocorreu" , "danger");
		btnLogin.val("Acessar");
		btnLogin.prop('disabled',false);
	}
}