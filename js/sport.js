var content_info = $(".content-info");


$(document).ready(function () {
	updateSport();
});

function updateSport() {
	getData("post", "",getUrlData(7), createTable);
}

function createTable() {
	mountList(3);
	hideLoading();
}

function addSport() {
	if($("#formAddSport input").eq(0).val() === ""){
		sendAlert("Digite um esporte","danger");
		return;
	}
	sendForm("formAddSport", getUrlData(7), successAdd);
}

function successAdd() {
	if(result_data.result === "ok"){
		sendAlert("Esporte adicionado", "success");
		getAllMenu(false);
		updateSport();
	} else {
		sendAlert("Erro ao adicionar esporte", "danger");
	}
}

function editSport(index) {
	content_info.css('display',"flex");
	let edit = $("#data");

	edit.html(
		"<input type='hidden' name='id' value='" + (result_data[index].id_sport) + "'>" +
		"<input type='hidden' name='type' value='edit'>" +
		"<li>Nome do esporte: <input type='text' name='name' value='" + result_data[index].name + "'></li>"
	);
}

function saveSport() {
	sendForm("formSport", getUrlData(7), successSave);
}

function successSave() {
	closeSport();
	getAllMenu();
	updateSport();
	if(result_data.result === "ok"){
		sendAlert("Esporte atualizado", "success");
	}else {
		sendAlert("Um erro ocorreu ao atualizar esporte", "danger");
	}
}

function deleteSport(index) {
	getData('post','type=delete&id='+result_data[index].id_sport, getUrlData(7),successDeleteSport);
}

function successDeleteSport() {
	if(result_data.result === "ok"){
		sendAlert("Esporte deletado com sucesso", "success");
		getAllMenu();
		updateSport();
	} else {
		sendAlert("Erro ao deletar", "danger");
	}
}

function closeSport() {
	content_info.css('display',"none");
}
