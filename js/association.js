let sel_ge_ed;
let sel_cl_ed;
let inputs;
let selects;

let form = $('#formAssociation');
let name;
let registry;
let email;
let birthday;
let number_phone;

$(document).ready(function () {
	setInterval(function () {
		getData("post",'',getUrlData(0), checkAssociation);
	}, 500);
	getAllMenu(true);
	sendAlert("Digite sua matricula para verificar cadastro");
	sendAlert("Caso contrário, apenas pressione o botão 'VERIFICAR'");

	setBgColor();
});


function checkAssociation() {
	if (result_data.enabled_association === "0"){
		window.location.href = "index.html";
	}
}

function verificationMember() {
	sendForm("formAssociation",getUrlData(5), successVerificationMember);
}

function successVerificationMember() {
	form.load('form_association.html', '', mountForm);
}

function mountForm(){
	sel_ge_ed = $("#sel_ge_ed");
	sel_cl_ed = $("#sel_cl_ed");

	name = $('input[name=name]');
	registry = $('input[name=registry]');
	email = $('input[name=email]');
	birthday = $('input[name=birthday_date]');
	number_phone = $("input[name='number_phone[]']");

	inputs = document.getElementsByTagName("input");
	selects = document.getElementsByTagName("select");


	if(result_data.result === "ok" && result_data.length > 0){
		sendAlert("Confirme seus dados e conclua a inscrição");
		sendAlert("Você já tem um registro!!!");
		oldMember();
	} else {
		newMember();
	}

	registry.mask('00000000',{clearIfNotMatch: true});

	number_phone.each(function(){
		$(this).mask('(00) 00000-0000', {clearIfNotMatch: true});
	});

	birthday.mask('00/00/0000', {clearIfNotMatch: true});

	$('#input-img').change(function(){
		$('#imgAdmin').html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});
}

function newMember() {
	form.append("<input type='hidden' name='user_type' value='2'>");

	mountSelectMenu('gender', null,'Selecione seu genero');
	mountSelectMenu('course', null, 'Selecione seu curso');
	mountSelectMenu('sport');
}

function oldMember() {
	name.val(result_data[0].name);
	registry.val(result_data[0].registry);
	email.val(result_data[0].email);
	birthday.val(result_data[0].birthday_date);
	number_phone.eq(0).val(result_data[0].number_1);
	number_phone.eq(1).val(result_data[0].number_2);

	form.append("<input type='hidden' name='id' value='" + result_data[0].id_user + "'>");

	$('.img').eq(0).attr('src',"img/users/"+result_data[0].path);
	$('input[name=type]').val("edit");

	for (let i = 0; i < menu_user_type.length; i++){
		if (menu_user_type[i].name === result_data[0].type){
			form.append("<input type='hidden' name='user_type' value='" + (i+1) + "'>");
			break;
		}
	}

	mountSelectMenu('gender', result_data[0].gender, 'Selecione seu genero');
	mountSelectMenu('course', result_data[0].course, 'Selecione seu curso');
	mountSelectMenu('sport', result_data[0].sport);
}

function validation(){
	let pass = true;
	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if(inputs.namedItem("imagem").files.item(0) === null && $('input[name=id]').length === 0){
		sendAlert("Escolha uma foto",'warning');
		pass = false;
	}

	if(registry.val() === ""){
		sendPopup(registry,"Preencha sua matricula",'warning', 'lc');
		pass = false;
	}

	if(name.val() === ""){
		sendPopup(name,"Preencha seu nome",'warning');
		pass = false;
	}

	if(birthday.val() === ""){
		sendPopup(birthday,"Preencha sua data de aniversário",'warning');
		pass = false;
	}

	if(email.val() === ""){
		sendPopup(email,"Preencha seu email",'warning','lc');
		pass = false;
	} else if(!re.test(String(email.val()).toLowerCase())){
		sendPopup(email,"Digite um email valido",'warning');
		pass = false;
	}

	if(number_phone.eq(0).val() === "" && number_phone.eq(1).val() === ""){
		sendPopup(number_phone.eq(0),"Preencha um numero celular",'warning');
		pass = false;
	}

	if(sel_cl_ed.val() === "null"){
		sendPopup(sel_cl_ed,"Preencha seu curso",'warning','lc');
		pass = false;
	}

	if(sel_ge_ed.val() === "null"){
		sendPopup(sel_ge_ed,"Preencha seu genero",'warning');
		pass = false;
	}

	return pass;
}

function addMember() {
	if(!validation()) return;
	sendForm("formAssociation", getUrlData(5),successAddMember, true);
}

function successAddMember() {
	sendAlert(result_data.msg, (result_data.result === "ok" ? "success" : "danger"));
}

function cancelMember() {
	window.location.href = "index.html";
}