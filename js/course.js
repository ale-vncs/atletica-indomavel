var edit = $("#data-course");
var content_info = $(".content-info");


$(document).ready(function () {
	updateCourse();
});

function updateCourse() {
	getData("post", "", getUrlData(8), createTable);
}

function createTable() {
	mountList(4);
	hideLoading();
}

function addCourse() {
	if($("#formAddCourse input[name=name]").val() === ""){
		sendAlert("Digite um curso","danger");
		return;
	}
	sendForm("formAddCourse", getUrlData(8), successAdd);
}

function successAdd() {
	if(result_data.result === "ok"){
		sendAlert("Curso atualizado", "success");
		getAllMenu();
		updateCourse();
	}else {
		sendAlert("Um erro ocorreu ao atualizar curso", "danger");
	}
}


function editCourse(index) {
	content_info.css('display','flex');
	edit.html(
		"<input type='hidden' name='id' value='" + (result_data[index].id_course) + "'>" +
		"<input type='hidden' name='type' value='member'>" +
		"<li>Nome do cargo: <input type='text' name='name' value='" + result_data[index].name + "'></li>"
	);
}

function saveCourse() {
	sendForm("formCourse", getUrlData(8), successSave);
}

function successSave() {
	closeCourse();
	if(result_data.result === "ok"){
		getAllMenu(false);
		updateCourse();
		sendAlert("Turma atualizado", "success");
	}else {
		sendAlert("Um erro ocorreu ao atualizar turma", "danger");
	}
}

function closeCourse() {
	content_info.css('display','none');
}

function deleteCourse(index) {
	getData("post", "type=delete&id="+result_data[index].id_course, getUrlData(8), successDeleteCourse);
}

function successDeleteCourse() {
	if(result_data.result === "ok"){
		sendAlert("Curso deletado com sucesso", "success");
		getAllMenu();
		updateCourse();
	} else {
		sendAlert("Erro ao deletar", "danger");
	}
}