var name_about = $('input[name=name]');
var title_about = $('input[name=title]');
var sub_title_about = $('input[name=sub_title]');
var association_about = $('input[name=association]');
var date_expiration_about = $('input[name=date_expiration]');
var textarea_about = $('textarea').eq(0);
var btn_about = $('input[name=btn]');

$('#input-img').change(function(){
	$('label').eq(0).html($(this).val().replace(/C:\\fakepath\\/i, ''));
});

$(document).ready(function () {
	getData("post","",getUrlData(0),setAbout);

	date_expiration_about.mask('00/00/0000', {clearIfNotMatch: true});
});

function setAbout() {
	name_about.val(result_data.name);
	title_about.val(result_data.title_index);
	sub_title_about.val(result_data.sub_title_index);
	association_about.prop('checked',result_data.enabled_association !== "0");
	textarea_about.val(result_data.about_text);
	date_expiration_about.val(result_data.date_expiration);
	date_expiration_about.val(result_data.date_expiration);

	$("input[name=color-primary]").spectrum("set", result_data.color_primary);
	$("input[name=color-secondary]").spectrum("set", result_data.color_secondary);
	$("input[name=color-text]").spectrum("set", result_data.color_text);
	hideLoading();
}

function sendAbout() {
	if(date_expiration_about.val() === ""){
		sendPopup(date_expiration_about,'Data obrigatória','danger');
		return;
	}
	sendForm("formAbout",getUrlData(0), successSend, true);
	btn_about.prop('disabled', true);
	btn_about.val("Aguarde...");
}

function successSend() {
	sendAlert(result_data.msg, (result_data.result === "ok" ? 'success' : 'danger'));
	btn_about.prop('disabled', false);
	btn_about.val("Salvar");
	if(result_data.result === "ok"){
		let time = 5;
		setInterval(function () {
			sendAlert("Página irá atualizar em " + time + " Seg!!!", 'info');
			time--;
			if(time === 0){
				window.location.reload();
			}
		},1000);
	}
}

$(".color").spectrum({
	showInput: true,
	chooseText: "Escolher",
	cancelText: "Cancelar",
	replacerClassName: 'none',
	containerClassName: 'none content-default',
	showInitial: true,
	move: function(color) {
		setColor($(this),color);
	},
	hide: function (color) {
		setColor($(this),color);
	}
});

function setColor(elem, color) {
	let names = ['color-primary','color-secondary','color-text'];
	for(let i = 0; i < names.length; i++){
		if(elem.attr('name') === names[i]){
			$("input[name="+names[i]+"]").val(color.toHexString());
			document.documentElement.style.setProperty('--'+names[i], color.toHexString());
		}
	}
}
