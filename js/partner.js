var content_info = $(".content-info");

$(document).ready(function () {
	getData("post","type=getPartner",getUrlData(9), startPartner);
	$('#input-img').change(function(){
		$("label").eq(0).html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});
});

function startPartner() {
	$("input [name=idLast]").val(result_data.length > 0 ? result_data[result_data.length-1].id_partner : '0');
	mountList(7);
}


function showPartnerAdd() {
	content_info.eq(0).css('display',"flex");
}

function addPartner() {
	sendForm("formPartnerAdd", getUrlData(9), successPartnerAdd, true);
}

function successPartnerAdd() {
	if(result_data.result === "ok"){
		sendAlert(result_data.msg, "success");
		getData("post","type=getPartner",getUrlData(9), startPartner);
	} else {
		sendAlert(result_data.msg, "danger");
	}
	closeInfo();
}

function editPartner(index) {
	content_info.eq(1).css('display',"flex");

	let data_partner = $("#edit-partner");
	data_partner.html(
		"<input name='type' type='hidden' value='updatePartner'>" +
		"<input name='imagem' type='hidden' value='" + result_data[index].path + "'>" +
		"<input name='id' type='hidden' value='" + result_data[index].id_partner + "'>"
	);

	data_partner.append(
		"<input id='input-img1' type='file' name='imagem'>" +
		"<label class='btn info' for='input-img1'><img src='../img/partner/" + result_data[index].path + "'></label>" +
		"<li>Nome do parceiro:<input type='text' name='name' value='" + result_data[index].name + "'></li>" +
		"<li>Sobre o parceiro:<input type='text' name='about' value='" + result_data[index].about + "'></li>"
	);

	$('#input-img1').change(function(){
		$("label").eq(1).html($(this).val().replace(/C:\\fakepath\\/i, ''));
	});
}

function deletePartner(index) {
	getData("post","type=delPartner&name="+result_data[index].path,getUrlData(9), successPartnerRem);
}

function successPartnerRem() {
	if(result_data.result === "ok"){
		sendAlert(result_data.msg, "success");
		getData("post","type=getPartner",getUrlData(9), startPartner);
	} else {
		sendAlert(result_data.msg, "danger");
	}
	closeInfo();
}

function savePartner() {
	sendForm("formPartnerUpdate", getUrlData(9),successPartner);
}

function successPartner() {
	if(result_data.result === "ok"){
		sendAlert("Parceiro atualizado", "success");
		getData("post","type=getPartner",getUrlData(9), startPartner);
	}
}

function closeInfo() {
	content_info.css('display',"none");
}
