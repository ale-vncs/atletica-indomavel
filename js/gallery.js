var btn_send_gallery = $('input [name=btn]');
var content_info = $(".content-info");

$(document).ready(function () {
	getImg();
});

function getImg() {
	getData("post", "type=getGallery", getUrlData(4), createList);
}

function createList() {
	mountList(6);
}

function add_photo() {
	let add = $(".add-photo");
	(add.css('bottom') === "0px" ? add.css('bottom', -add.height() + "px") : add.css('bottom', 0));
}

function sendImgGallery(){
	if (document.getElementById("input-img").files.length === 0){
		sendAlert("Selecione alguma imagem", "danger");
		return;
	}
	btn_send_gallery.prop('disabled', true);
	btn_send_gallery.val("Enviando...");
	sendForm("formGallery",getUrlData(4), successSendImg, true);
}

function successSendImg(){
	if(result_data.result === "ok"){
		(page_index === 1) ? getImg() : sendAlert("Retorne a galeria para visualizar as imagens", "info");
		sendAlert(result_data.msg, (result_data.result === "ok" ? "success" : "danger"));
	}
	btn_send_gallery.val("Enviar");
	btn_send_gallery.prop('disabled', false);
}


document.getElementById('input-img').addEventListener('change', function(){
	let files = document.getElementById("input-img").files;
	let field = document.getElementById('file-name');
	field.innerHTML = "";
	for (let i = 0; i < files.length; i++) {
		field.innerHTML += "<li>" + files[i].name + "</li>";
	}
});

function deleteGallery(index) {
	getData("post", "type=delGallery&name=" + result_data[index].path, getUrlData(4), remSuccess);
}

function remSuccess() {
	if (result_data.result === "ok"){
		sendAlert("Imagem " + result_data.name +" removida!", "success");
		getImg();
	} else {
		sendAlert("Erro ao deletar imagem " + result_data.name, "danger");
	}
}

function onScroll() {
	if (!isScrolledIntoView('#top')) {
		$(".toTop").eq(0).css('bottom', '40px');
	} else {
		$(".toTop").eq(0).css('bottom', '-150px');
	}
}