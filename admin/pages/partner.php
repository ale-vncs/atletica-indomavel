<div class="flex-v center-v">
	<div class="btn success" onclick="showPartnerAdd()"><i class="fas fa-plus margin-right"></i> Adicionar</div>
	<div class="list"></div>
</div>

<div class="content-info flex-v">
	<div class="content-default flex-v center-v">
		<form id="formPartnerAdd">
			<input type="hidden" name="type" value="setPartner">
			<input type="hidden" name="idLast">
			<input id="input-img" name="imagem" type="file"><label class="btn info" for="input-img">Selecione uma imagem</label>
			<input type="text" name="name" placeholder="Nome do parceiro">
			<textarea name="about" placeholder="Sobre o parceiro"></textarea>
		</form>
		<div>
			<input class='btn success' type='button' value='Adicionar' onclick='addPartner()'>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo(0)'>
		</div>
	</div>
</div>

<div class="content-info flex-v">
	<div class="more-info">
		<form id="formPartnerUpdate">
			<ul id="edit-partner"></ul>
		</form>
		<div>
			<input class='btn success' type='button' value='Salvar' onclick='savePartner()'>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo(1)'>
		</div>
	</div>
</div>

<div class="content-info flex-v">
	<div id="remPartner" class="more-info">

	</div>
</div>


<script src="../js/partner.js?<?php echo date("ymdHis"); ?>"></script>
