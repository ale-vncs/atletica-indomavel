<div class="content-default flex-v center-v">
	<form id="formAlertAdd">
		<input type="hidden" name="type" value="addAlert">

		<input id="input-img" name="imagem" type="file"><label class="btn info" for="input-img" id="imgAdmin">Selecione uma imagem</label>
		<input name="titleAlert" type="text" placeholder="titulo">
		<textarea rows='4' name="messageAlert" placeholder="mensagem"></textarea>
		<div>
			<label for="calendar">Dia de expiração</label>
			<div id="calendar"></div>
		</div>
		<div>
			<label>Prioridade</label>
			<select name="priorityAlert"></select>
		</div>
		<div class="btn success" onclick="addAlert()">Adicionar</div>
	</form>
</div>

<div class="list"></div>

<div class="content-info">
	<div class="more-info">
		<form id="formAlertUpdate">
			<ul id="edit-alert"></ul>
		</form>
		<div>
			<input class='btn success' type='button' value='Salvar' onclick='saveAlert()'>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo()'>
		</div>
	</div>
</div>

<script src="../js/calendar.js"></script>
<script src="../js/alert.js?<?php echo date("ymdHis"); ?>"></script>
