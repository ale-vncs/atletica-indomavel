
<div class="content-default flex-v center-v">
	<form id="formLog">
		<div>
			<label for="sel_se">Seção</label>
			<select class="margin-right" id="sel_se" name="section"></select>
			<label for="sel_ac">Ação</label>
			<select class="margin-right" id="sel_ac" name="action"></select>
		</div>
		<label for="calendar">Data da Atividade</label>
		<div id="calendar"></div>

		<div>
			<input class="margin-right" type="text" name="registry" placeholder="Matricula">
			<input class="margin-right" type="text" name="name" placeholder="Nome">
		</div>
	</form>
	<div class="btn info" onclick="searchLog()">Buscar</div>
</div>

<div class="list"></div>

<script src="../js/log.js?<?php echo date("ymdHis"); ?>"></script>
<script src="../js/calendar.js"></script>