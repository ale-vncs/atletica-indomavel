
<div class="content-default flex-v center-v">
	<form id="formMember">
		<input type="hidden" name="type" value="general">
		<div>
			<label for="sel_se">Esporte</label>
			<div class="custom-select"><select class="margin-right" id="sel_sp" name="sport"></select></div>

			<label for="sel_ac">Curso</label>
			<select class="margin-right" id="sel_cl" name="class"></select>
		</div>
		<div>
			<input type="text" name="registry" placeholder="Matrícula">
			<input type="text" name="name" placeholder="Nome">
		</div>
		<label for="calendar">Data de Cadastro</label>
		<div id="calendar"></div>
		<div>
			<label for="ass">Tipo de Associados</label>
			<select id="ass" name="association">
				<option value="2">Todos</option>
				<option value="1">Associado</option>
				<option value="0">Não Associado</option>
			</select>
		</div>
		<div class="btn danger" onclick="sendFormMember()">Buscar</div>
	</form>
</div>

<div class="list"></div>

<div class="content-info flex-v">
	<div class="more-info">
		<div class="margin-bottom">
			<ul id="data-user"></ul>
		</div>
		<div class="btn danger" onclick="closeInfo()"><i class="fas fa-times"></i></div>
	</div>
</div>

<div class="content-info">
	<div class="more-info">
		<form id="formMemberUpdate">
			<ul id="edit-member"></ul>
			<div>
				<input class='btn success' type='button' value='Salvar' onclick='saveMember()'>
				<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo()'>
			</div>
		</form>
	</div>
</div>

<script src="../js/member.js?<?php echo date("ymdHis"); ?>"></script>
<script src="../js/calendar.js"></script>