
<div class="home-content">
	<div class="home-row">
		<div class="home-card content-default">
			<a class="fas fa-user-friends"></a>
			<h4>Membros</h4>
			<p id="countUsers">0</p>
		</div>
		<div class="home-card content-default">
			<a class="fas fa-exclamation-circle"></a>
			<h4>Alertas</h4>
			<p id="countAlert">0</p>
		</div>
		<div class="home-card content-default">
			<a class="fas fa-handshake"></a>
			<h4>Parceiros</h4>
			<p id="countPartner">0</p>
		</div>
	</div>
	<div class="home-row">
		<div class="home-graph content-default">
			<canvas id="graph_gender"></canvas>
		</div>
		<div class="home-graph content-default">
			<canvas id="graph_course"></canvas>
		</div>
		<div class="home-graph content-default">
			<canvas id="graph_sport"></canvas>
		</div>
		<div class="home-graph content-default">
			<a class='checkboxes-and-radios'>
				<div class="flex-h"><input type='checkbox' name='by_date' id="by_date"><label for='by_date'>Por dia</label></div>
			</a>
			<canvas id="graph_visit"></canvas>
		</div>
	</div>
	<div class="list"></div>
</div>
<script src="../js/home.js?<?php echo date("ymdHis"); ?>"></script>
