
<div class="content-default flex-v center-v center-h">
	<form id="formOfficeAdd">
		<input type="text" name="name" placeholder="Nome do cargo">
		<input type="hidden" name="action" value="insert">
		<label>Adicionar em:</label>
		<select name="type">
			<option value="user_type" selected>Membro</option>
			<option value="admin_type">Diretor</option>
		</select>
		<div class="btn success" onclick="addOffice()">Adicionar</div>
	</form>
</div>
<!--<div class="tab-container">
	<div class="tab-container-button">
		<div class="tab-button">Membro</div>
		<div class="tab-button">Diretor</div>
	</div>
	<div class="tab-container-box">
		<div class="tab-box">
			<section>
				<input type="text" name="name" placeholder="Cargo">
				<div class="btn success">Adicionar</div>
			</section>
		</div>
		<div class="tab-box">
			<section>
				2
			</section>
		</div>
	</div>
</div>-->

<div class="content-info flex-v">
	<div class="more-info">
		<form id="formOfficeUpdate">
			<ul id="edit-office"></ul>
		</form>
		<div>
			<input class='btn success' type='button' value='Salvar' onclick='saveOffice()'>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeOffice()'>
		</div>
	</div>
</div>

<div class="list"></div>

<script src="../js/office.js"></script>