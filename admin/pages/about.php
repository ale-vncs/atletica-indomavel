<div class="content-default flex-v center-v">
	<form id="formAbout">
		<input type="hidden" name="type" value="about">
		<input type="file" name="imagem" id="input-img"><label class="btn info margin-bottom" for="input-img"><img class="margin-bottom img" src="../img/loading.png"></label>
		<input type="text" name="name" placeholder="Nome da Atlética">
		<input type="text" name="title" placeholder="Titulo da página inicial">
		<input type="text" name="sub_title" placeholder="Subtitulo da página inicial">
		<textarea rows="4" name="about" placeholder="Sobre a atlética"></textarea>
		<div class="margin-top">
			<div class="checkboxes-and-radios">
				<input type="checkbox" name="association" id="assoc"><label for="assoc">Habilitar associação</label>
			</div>
		</div>
		<div>
			<label>Data de associação</label>
			<input type="text" name="date_expiration">
		</div>

		<div>
			<label>Cor primaria</label>
			<input class="color" type='text' name="color-primary" >
		</div>
		<div>
			<label>Cor secundaria</label>
			<input class="color" type='text' name="color-secondary">
		</div>
		<div>
			<label>Cor do texto</label>
			<input class="color" type='text' name="color-text">
		</div>
		<input class="btn success" type="button" name="btn" value="Salvar" onclick="sendAbout()">
	</form>
</div>

<script src="../js/about.js?<?php echo date("ymdHis"); ?>"></script>