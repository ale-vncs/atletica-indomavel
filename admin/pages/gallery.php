<div id="top"></div>

<div class="content-gallery" onscroll="onScroll()">
	<div class="btn success" onclick="add_photo()"><i class="fas fa-plus margin-right"></i> Adicionar Foto</div>
	<div class="list"></div>
</div>

<div class="add-photo">
	<i class="fas fa-times" onclick="add_photo()"></i>
	<div class="form flex-v center-v">
		<form id="formGallery" class="flex-v center-v center-h" enctype="multipart/form-data">
			<input type="hidden" name="type" value="addGallery">
			<div class="flex-h margin-bottom">
				<label for="input-img" class="btn info">Selecione a Imagem</label>
				<input type="file" id="input-img" name="imagem[]" value="arquivo" multiple required>
			</div>
			<ul id="file-name" class="flex-v"><li>Sem imagem</li></ul>
			<input type="button" name="btn" class="btn success" onclick="sendImgGallery()" value="Enviar">
		</form>
	</div>
</div>

<a class="toTop" href="#top"><i class="fas fa-caret-up"></i></a>

<script src="../js/gallery.js?<?php echo date("ymdHis"); ?>"></script>
