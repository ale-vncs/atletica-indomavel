<div class="flex-v center-v">
	<div class="btn success" onclick="showDirectorAdd()"><i class="fas fa-plus margin-right"></i> Adicionar</div>
	<div class="list"></div>
</div>

<div class="content-info flex-v">
	<div class="more-info">
		<form id="formDirectorAdd">
			<input type="hidden" name="type" value="setDirector">
			<select class="sel_mb_ed" name="memberId"></select>
			<select class="sel_at_ed" name="memberType"></select>
		</form>
		<div>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo(0)'>
			<input class='btn success' type='button' value='Adicionar' onclick='addDirector()'>
		</div>
	</div>
</div>

<div class="content-info flex-v">
	<div class="more-info">
		<form id="formDirectorUpdate">
			<ul id="edit-director"></ul>
		</form>
		<div>
			<input class='btn danger' type='button' value='Cancelar' onclick='closeInfo(1)'>
			<input class='btn success' type='button' value='Salvar' onclick='saveDirector()'>
		</div>
	</div>
</div>

<div class="content-info flex-v">
	<div id="remDirector" class="more-info">

	</div>
</div>

<script src="../js/director.js?<?php echo date("ymdHis"); ?>"></script>