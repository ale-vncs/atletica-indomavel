<div class="content-default flex-v center-v">
	<form id="formAccount">
		<input id="input-img" name="imagem" type="file"><label class="btn info" for="input-img" id="imgAdmin"><img class="img" src="../img/users/unknown_people.png"></label>
		<input type="hidden" name="remImg" value="false">
		<div class="btn danger" onclick="removeImg()">Remover Imagem</div>
		<div>
			<input type="text" name="registry" placeholder="Matricula">
			<input type="text" name="name" placeholder="Nome">
		</div>
		<div>
			<input type="text" name="birthday_date" placeholder="Data de nascimento">
			<input type="text" name="email" placeholder="Email">
		</div>
		<div>
			<select class="sel_ge_ed" name="gender"></select>
			<select class="sel_cl_ed" name="course"></select>
		</div>
		<div>
			<label for="sel_sp_ed">Esporte:</label>
			<div class='checkboxes-and-radios sel_sp_ed'></div>
		</div>
		<div>
			<input type="password" name="password_new" placeholder="Nova senha">
			<input type="password" name="password_new_again" placeholder="Digite novamente">
		</div>
		<textarea rows="4" name="about" placeholder="Sobre mim"></textarea>
		<input class="margin-top" type="password" name="password_current" placeholder="Digite sua senha atual">
		<input type="button" name="btn" class="btn success" onclick="setAccount()" value="Salvar">
	</form>
</div>

<script src="../js/account.js?<?php echo date("ymdHis"); ?>"></script>