<div class="content-default flex-v center-v center-h">

	<form id="formAddCourse">
		<div>
			<input type="text" name="name" placeholder="Nome do curso">
			<input type="hidden" name="type" value="new">
		</div>
	</form>
	<input type="button" onclick="addCourse()" class="btn success" value="Adicionar">

</div>

<div class="list"></div>

<div class="content-info">
	<div class="more-info">
		<form id="formCourse" class="flex-h margin-bottom">
			<ul id="data-course"></ul>
		</form>
		<div>
			<input type='button' value='Salvar' class='btn success' onclick='saveCourse()'>
			<input type='button' value='Cancelar' class='btn danger' onclick='closeCourse()'>
		</div>
	</div>
</div>

<script src="../js/course.js?<?php echo date("ymdHis"); ?>"></script>