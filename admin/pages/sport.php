<div class="content-default flex-v center-v center-h">
	<form id="formAddSport">
		<div>
			<input type="text" placeholder="Nome do esporte" name="name">
			<input type="hidden" name="type" value="new">
		</div>
	</form>
	<div onclick="addSport()" class="btn success" >Adicionar</div>
</div>

<div class="list"></div>

<div class="content-info">
	<div class="more-info">
		<form id="formSport" class="flex-h margin-bottom">
			<ul id="data"></ul>
		</form>
		<div>
			<input type='button' value='Salvar' class='btn success' onclick='saveSport()'>
			<input type='button' value='Cancelar' class='btn danger' onclick='closeSport()'>
		</div>
	</div>
</div>


<script src="../js/sport.js?<?php echo date("ymdHis"); ?>"></script>