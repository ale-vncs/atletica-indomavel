<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Dashboard</title>

		<link rel="stylesheet" href="../css/dashboard.css?<?php echo date("ymdHis"); ?>">
		<link rel="stylesheet" href="../js/spectrum/spectrum.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">
		<link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="../js/mask/jquery.mask.js"></script>
		<script src="../js/spectrum/spectrum.js"></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.8.2/countUp.min.js'></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
		<script src="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"></script>

		<script src="../js/variables.js?<?php echo date("ymdHis"); ?>"></script>
		<script src="../js/function_start.js?<?php echo date("ymdHis"); ?>"></script>
		<script src="../js/dropdown.js?<?php echo date("ymdHis"); ?>"></script>

	</head>
	<body onresize="onresize()">
		<div class="bg"></div>
		<ul id="alert"></ul>
		<div id="progressBar"></div>
		<div class="main">
			<div class="side-bar">
				<div class="side-bar-title">
					<p>Atlética Indomável</p>
				</div>
				<div class="side-bar-header">
					<img id="user_img" alt="" src="../img/users/unknown_people.png" onclick="testScript()">
					<h3></h3>
					<h4></h4>
				</div>
				<div class="side-bar-body">
					<div class="side-bar-menu"></div>
				</div>
				<div style='flex: 1; width: 100%; position: relative; background: var(--color-opacity-black);'></div><!--Isso é uma div de suporte ao background-->
			</div>
			<div class="content-main">
				<div class="header">
					<a id="menu-btn" class="fas fa-bars" onclick="menu_side()"></a>
					<p id="name-page"></p>
					<a class="fas fa-power-off" href="../connection/logoff.php" ></a>
				</div>
				<div class="page">
					<div id="page"></div>
				</div>
			</div>
		</div>
		<script src="../js/dashboard.js?<?php echo date("ymdHis"); ?>"></script>
	</body>
</html>