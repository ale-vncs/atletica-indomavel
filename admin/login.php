<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Login</title>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="../js/variables.js?<?php echo date("ymdHis"); ?>"></script>
		<script src="../js/function_start.js?<?php echo date("ymdHis"); ?>"></script>
		<link rel="stylesheet" href="../css/login.css?<?php echo date("ymdHis"); ?>">
	</head>

	<body>
		<div class="bg"></div>
		<div id="alert"></div>
		<div class="content">
			<div class="login">
				<img src="../img/loading.png">
				<form class="flex-v margin-bottom" id="formLogin">
					<input id="login" type="text" placeholder="Matrícula" name="registry" maxlength="8">
					<input id="password" type="password" placeholder="Senha" name="password">
					<input type="button" id="btnLogin" class="btn success margin-top" onclick="onLogin()" value="Acessar">
				</form>
			</div>
		</div>
		<script src="../js/login.js?<?php echo date("ymdHis"); ?>"></script>
	</body>
</html>