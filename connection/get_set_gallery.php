<?php
	include('check_session.php');


	$data = array();

	$arquivo = isset($_FILES['imagem']) ? $_FILES['imagem'] : FALSE;
	$diretorio = $path_gallery;

	$type = $_POST['type'];

	if ($type == "addGallery") { //====================================Adicionar imagem na galeria
		if(!isset($_FILES['imagem'])){
			$data["result"] = "error";
			$data["msg"] = "Selecione uma imagem";
			echo json_encode($data);
			exit();
		}
		for ($k = 0; $k < count($arquivo['name']); $k++) {
			$count = glob("$diretorio{*.jpg,*.jpeg,*.JPG,*.png,*.gif,*.bmp}", GLOB_BRACE);
			$extensao = strtolower(end(explode('.', $arquivo['name'][$k])));
			$name = "G_" . (count($count) + 1) . "_" . date('d-m-Y_H-i-s', time()) . '.' . $extensao;
			$extensoes = array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'JPG');     // extensoes permitidas
			if (!in_array($extensao, $extensoes))
				continue;

			$destino = $diretorio . $name;


			if (move_uploaded_file($arquivo['tmp_name'][$k], $destino)) {
				$data['photo'][$k] = $arquivo['name'][$k] . " adicionado";
				$pdo->prepare("insert into gallery_img (path) value ('$name')")->execute();
				setLog('gallery','add','Imagem adicionada');
			} else {
				$data['photo'][$k] = $arquivo['name'][$k] . " -> Não foi possivel enviar imagem";
				$data['error'] = $pdo->errorInfo();
			}
		}
		$data['length'] = count($arquivo['name']);
		$data['result'] = "ok";
		$data['msg'] = "Imagens adicionadas";
	} else if($type == "delGallery"){ //======================================Delelar Imagem da galeria
		$name = $data['name'] = $_POST['name'];
		if(unlink($diretorio.$name)){
			$pdo->prepare("delete from gallery_img where path='$name'")->execute();
			setLog('gallery','rem','Imagem apagada');
			$data['result'] = "ok";
		} else {
			$data['result'] = "error";
		}
	} else if($type == "getGallery") { //===============================Receber imagem
		$sql = $pdo->prepare("select * from gallery_img");

		if($sql->execute()){
			$g = 0;
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		}

		if($data == []){
			$data["result"] = "error";
		}else{
			$data["length"] = count($data);
			$data["result"] = "ok";
		}
	} else {
		$data['result'] = "error";
		$data['msg'] = "Paramentro TYPE não passado";
	}

	echo json_encode($data);

?>