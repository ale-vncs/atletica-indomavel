<?php
	include('check_session.php');


	$type = $_POST['type'];

	if($type == "getDirector"){
		$registry = $name = "";


		if (isset($_POST['registry']))
			$registry = " and us.registry like '%" . $_POST['registry'] . "%'";

		if (isset($_POST['name']))
			$name = " and us.name like '%" . $_POST['name'] . "%'";


		$data = array();



		$fr = "select  us.id_user,
       				   us.registry,
					   us.name,
					   us.path,
					   ifnull(ua.about,'Sem comentario') as about,
					   (select name from admin_type where id_type = ua.id_type) as type
				from users us
				inner join user_admin ua on us.id_user = ua.id_user
				inner join admin_type at on us.id_type = at.id_type
				where 0=0 and ua.id_type <> 1" . $registry . $name .
			" order by us.name asc"

		;

		$sql = $pdo->prepare($fr);

		$g = 0;
		if($sql->execute()){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		}

		if($data == []){
			$data["result"] = "error";
		}else{
			$data["length"] = count($data);
			$data["result"] = "ok";
		}

	} else if($type == "setDirector"){
		$memberId = $_POST['memberId'];
		$memberType = $_POST['memberType'];

		if($memberId == null || $memberType == null){
			echo json_encode(array("result"=>"error","msg"=>"Preencha todos os campos"));
			exit();
		}

		$pdo->beginTransaction();

		$sql_1 = $pdo->prepare("insert into user_admin (id_user, id_type) value ($memberId, $memberType)")->execute();
		$sql_2 = $pdo->prepare("insert into user_attempts_login (id_user) value ($memberId)")->execute();
		$sql_3 = $pdo->prepare("update users set is_admin = 1 where id_user = $memberId")->execute();

		if($sql_1 && $sql_2 && $sql_3){
			$data["result"] = "ok";
			$data["msg"] = "Diretor adicionado";
			$pdo->commit();
			setLog('director','add','Diretor adicionado');
		} else {
			$data["result"] = "error";
			$data["msg"] = "Algum erro ocorreu";
			$data["sql"] = $sql_1 . " - " . $sql_2 . " - " . $sql_3;
			$pdo->rollBack();
		}
	} else if($type == "updateDirector"){
		$sql = $pdo->prepare("update user_admin set id_type = " . $_POST['id_type'] . " where id_user = " . $_POST['id_user']);
		$data['de'] = $_POST['id_type'] . " - " . $_POST['id_user'];
		if($sql->execute()){
			$data["result"] = "ok";
			$data["msg"] = "Diretor(a) atualizado";
		} else {
			$data["result"] = "error";
			$data["msg"] = "Erro ao atualizar";
		}
	} else if($type == "delDirector"){
		$pdo->beginTransaction();
		$sql_1 = $pdo->prepare("delete from user_attempts_login where id_user = " . $_POST['id_user'])->execute();
		$sql_2 = $pdo->prepare("delete from user_admin where id_user = " . $_POST['id_user'])->execute();
		$sql_3 = $pdo->prepare("update users set is_admin = 0 where id_user = " . $_POST['id_user'])->execute();

		if($sql_1 && $sql_2 && $sql_3){
			$data["result"] = "ok";
			$data["msg"] = "Diretor(a) removido";
			$pdo->commit();
			setLog('director','rem','Diretor removido');
		} else {
			$data["result"] = "error";
			$data["msg"] = "Erro ao remover";
			$pdo->rollBack();
		}
	}else {
		$data["result"] = "error";
		$data["msg"] = "paramentro type não enviado";
	}

	echo json_encode($data);
?>