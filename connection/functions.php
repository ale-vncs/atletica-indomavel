<?php

	function setLog($nameSection, $nameAction, $message){
		$action_data['add'] = 1;
		$action_data['rem'] = 2;
		$action_data['mod'] = 3;
		$action_data['acc'] = 4;

		$section_data['home'] = 1;
		$section_data['gallery'] = 2;
		$section_data['director'] = 3;
		$section_data['member'] = 4;
		$section_data['alert'] = 5;
		$section_data['sport'] = 8;
		$section_data['course'] = 9;
		$section_data['about'] = 10;
		$section_data['account'] = 11;
		$section_data['partner'] = 12;

		Conexao::getInstance()->prepare("insert into log (id_user, id_section, id_action, message) value ('" . $_SESSION['data']['id_user'] . "',". $section_data[$nameSection] .", ". $action_data[$nameAction] .", '$message' )")->execute();
	}

	//check member association
	function checkAssociationMember(){
		$temp = Conexao::getInstance()->prepare("select date_expiration from about");
		$temp->execute();
		$date_expiration = $temp->fetch(PDO::FETCH_ASSOC)['date_expiration'];

		if(strtotime($date_expiration) < strtotime(date('Y-m-d'))){
			Conexao::getInstance()->prepare("update users set enabled = 0")->execute();
		}
	}

	function convertDateBrToDB($date){ //Converte data do normal para o banco de dados
		return DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
	}

	function convertDateDBToBr($date){ //Converte data do banco para o normal
		return DateTime::createFromFormat('Y-m-d', $date)->format('d/m/Y');
	}
