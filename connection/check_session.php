<?php
	require_once 'connection.php';
	$pdo = Conexao::getInstance();


	include('imageLib.php');
	include('functions.php');

	session_start();

	checkAssociationMember(); //Checa a associação de todos os membros


	//Verifica se a origem da requisição é do mesmo domínio da aplicação
	if (!(isset($_SERVER['HTTP_REFERER']) && ($_SERVER['HTTP_HOST'] == $host_default || $_SERVER['HTTP_HOST'] == $host_debug))){
		sendError("Origem da requisição não autorizada!");
	}


	$host = $pages = array();

	$pages[] = "/atletica-indomavel/admin/login.php";
	$pages[] = "/atletica-indomavel/admin/dashboard.php";
	$pages[] = "/atletica-indomavel/home.php";
	$pages[] = "/atletica-indomavel/association.html";

	for($i = 0; $i < count($pages); $i++){
		$host[] = [$host_default . $pages[$i],$host_debug . $pages[$i]];
	}


	$pass_page = false;
	$current_host = str_replace(array('http://','https://') ,'',$_SERVER['HTTP_REFERER']);
	for($i = 0; $i < count($host); $i++){
		if($current_host == $host[$i][0] || $current_host == $host[$i][1]){
			$pass_page = true;
			if($current_host == $host[0][0] || $current_host == $host[0][1]){
				if(isset($_SESSION['data'])){
					echo json_encode(array("page"=>"dashboard.php"));
					exit();
				}
			} else if($current_host == $host[1][0] || $current_host == $host[1][1]){
				if(!isset($_SESSION['data'])){
					echo json_encode(array("page"=>"login.php"));
					exit();
				}
			}
		}
	}

	if(!$pass_page){
		sendError("Pagina não autorizada");
	}
?>

