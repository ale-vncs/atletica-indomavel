<?php

	include('check_session.php');

	$data = array();

	$type = $_POST['type'];

	if($type == ''){
		$fr = "select * from sport";
	} else if($type == "edit") {
		$fr = "update sport set name='". $_POST['name'] . "' where id_sport=" . $_POST['id'];
		setLog('sport','mod','Esporte atualizado');
	} else if($type == "delete") {
		$fr = "delete from sport where id_sport=" . $_POST['id'];
		setLog('sport','rem','Esporte removido');
	} else if($type == "new"){
		$fr = "insert into sport (name) value ('" . $_POST['name'] . "')";
		setLog('sport','add','Esporte adicionado');
	}

	$sql = $pdo->prepare($fr);
	$g = 0;
	if($sql->execute()){
		if($type == ""){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		} else {
			$data['result'] = "ok";
		}
	}

	if($data == []){
		$data['result'] = "error";
		$data['msg'] = "Sql não executado";
	} else {
		$data['length'] = count($data);
		$data['result'] = "ok";
		$data['msg'] = "Sql concluido com sucesso";
	}

	echo json_encode($data);