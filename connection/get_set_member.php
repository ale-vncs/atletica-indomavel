<?php
	include('check_session.php');
	$data = array();
	$type = $_POST['type'];
	if($type == "general"){
		$filter = $sport = $class = $registry = $name = $date = $association = "";

		if(!isset($_POST['all']))
			$filter = 'and us.is_admin = 0';
		else
			if(empty($_POST['registry']))
				$_POST['registry'] = -1;

		if(isset($_POST['sport']) && $_POST['sport'] != 0)
			$sport = " and sp.id_sport = " . $_POST['sport'];

		if (isset($_POST['course'])  && $_POST['course'] != 0)
			$course = " and us.course = " . $_POST['course'];

		if (isset($_POST['registry'])  && !empty($_POST['registry']))
			$registry = " and us.registry like '%" . $_POST['registry'] . "%'";

		if (isset($_POST['day']) && $_POST['day'] != 0 && isset($_POST['mouth']) && $_POST['mouth'] != 0 && isset($_POST['year']) && $_POST['year'] != 0)
			$date = " and date_format(us.join_date,'%d/%m/%Y') = '" . $_POST['day'] . "/" . $_POST['mouth'] . "/" . $_POST['year'] . "'";

		if (isset($_POST['name']) && !empty($_POST['name']))
			$name = " and us.name like '%" . $_POST['name'] . "%'";

		if (isset($_POST['association']) && $_POST['association'] != 2)
			$association = " and us.enabled = " . $_POST['association'];

		$fr = "select distinct 		us.id_user,
                					us.registry,
									us.name,
                					us.email,
                					us.path,
                					us.enabled,
									ifnull((select name from user_type where id_type = us.id_type),'Sem tipo') as type,
                					ifnull((select gender from gender where gender.id_gender = us.id_gender),'Sem genero') as gender,
                					un.number_1,
               						un.number_2,
									ifnull(group_concat((select name from sport where id_sport = usp.id_sport group by sp.name)), 'Sem Esporte') as sport,
									ifnull((select name from course where id_course = us.id_course group by cl.name), 'Sem Curso') as course,
									date_format(us.birthday_date,'%d/%m/%Y') as birthday_date,
									date_format(us.join_date,'%d/%m/%Y -- %T') as join_date,
									date_format(us.association_date,'%d/%m/%Y') as association_date,
									if(us.enabled = 0, 'Não', 'Sim') as association
					from users us
					left join user_sport usp on us.id_user = usp.id_user
					left join course cl on us.id_course = cl.id_course
					left join sport sp on usp.id_sport = sp.id_sport
					left join user_type ust on us.id_type = ust.id_type
					left join user_number un on us.id_user = un.id_user
					where 0=0 ". $filter . $sport . $registry . $course . $date . $name . $association .
			" group by us.registry order by us.name";

		$sql = $pdo->prepare($fr);

		$g = 0;
		if($sql->execute()){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		}


		if($data == []){
			$data["result"] = "error";
			$data["error"] = $pdo->errorInfo();
			//$data['fr'] = $fr;
		}else{
			$data["length"] = count($data);
			$data["result"] = "ok";
		}

		echo json_encode($data);

	} else if ($type == "edit"){

		$pdo->beginTransaction();

		$stmt_1 = $pdo->prepare("delete from user_sport where id_user = '" . $_POST['id'] . "'")->execute();

		$fr = " update users
				set registry = '" . $_POST['registry'] . "', 
					name = '" . $_POST['name'] . "', 
					email = '" . $_POST['email'] . "', 
					id_gender = " . $_POST['gender'] . ", 
					birthday_date = date_format(str_to_date('" . $_POST['birthday_date'] . "','%d/%m/%Y'),'%Y-%m-%d'),
					id_type = " . $_POST['user_type'] . ",
					id_course = " . $_POST['course'] . ",
					enabled = " . (isset($_POST['enabled']) ? '1' : '0') . "
				where id_user = '" . $_POST['id'] . "'
				";

			//$data['fr'] = $fr;
		$stmt_2 = $pdo->prepare($fr)->execute();


		$fr = "";
		for($i = 0; $i < count($_POST['sport']); $i++){
			$fr .= "('" . $_POST['id'] . "', '" . $_POST['sport'][$i] . "'),";
		}
		if($fr == ""){
			$stmt_3 = true;
		} else {
			$fr = substr($fr,0, strlen($fr)-1);
			$stmt_3 = $pdo->prepare("insert into user_sport (id_user, id_sport) value " . $fr)->execute();
		}

		$stmt_4 = $pdo->prepare("insert into user_number (id_user, number_1 , number_2) value ('" . $_POST['id'] ."','" . $_POST['number_phone'][0] . "', '" . $_POST['number_phone'][1] . "')")->execute();
		if(!$stmt_4)
			$stmt_4 = $pdo->prepare("update user_number set number_1 = '" . $_POST['number_phone'][0] . "', number_2 = '" . $_POST['number_phone'][1] . "' where id_user = " . $_POST['id'])->execute();

		if($stmt_1 && $stmt_2 && $stmt_3 && $stmt_4){

			if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
				$arquivo = $_FILES['imagem'];
				$extensao = strtolower(end(explode('.', $arquivo['name'])));
				$name = "img_" . $_POST['id'] . "." . $extensao;
				$destino = $path_users . $name;
				move_uploaded_file($arquivo['tmp_name'], $destino);
				$resizeObj = new imageLib($destino);
				$resizeObj -> resizeImage(500, 500, 'crop');
				$resizeObj -> saveImage($destino, 100);
				$pdo->prepare("update users set path = '" . $name . "' where id_user = " . $_POST['id'])->execute();
			} else {
				$resizeObj = true;
			}

			if($resizeObj){
				$pdo->commit();
				setLog('member','mod','Membro alterado');
				$data["result"] = "ok";
				$data['msg'] = "Membro alterado";
			} else {
				$pdo->rollBack();
				$data["result"] = "error";
				$data['msg'] = "Erro ao salvar imagem";
			}
		}else{
			$pdo->rollBack();
			$data["result"] = "error";
			$data['msg'] = "Erro ao tentar atualizar";
		}
		if($stmt_1) $data['st1'] = true;
		if($stmt_2) $data['st2'] = true;
		if($stmt_3) $data['st3'] = true;
		if($stmt_4) $data['st4'] = true;
		$data['nm'] = $_POST['number_phone'][1];

		echo json_encode($data);
		exit();
	} else if($type == "add"){
		$pdo->beginTransaction();


		$fr = "insert into users (registry, name, email, id_gender, birthday_date, association_date, id_type, id_course) value 
    	(
    	    " . $_POST['registry'] . ",
    	    '" . $_POST['name'] ."',
    	    '" . $_POST['email'] . "',
    	    " . $_POST['gender'] . ",
    	    date_format(str_to_date('" . $_POST['date'] . "','%d/%m/%Y'),'%Y-%m-%d'),
    	    current_timestamp,
    	    2,
    	    " . $_POST['course'] . "
    	)";
		$data['1'] = $fr;
		$stmt_1 = $pdo->prepare($fr)->execute();

		$id = $pdo->lastInsertId();

		$fr = "('" . $id . "', " . (isset($_POST['number_phone'][0]) ? "'" . $_POST['number_phone'][0] . "'" : null) . ", ". (isset($_POST['number_phone'][0]) ? "'" . $_POST['number_phone'][1] . "'" : null) .")";
		$stmt_2 = $pdo->prepare("insert into user_number (id_user, number_1, number_2) value " . $fr)->execute();

		$data['2'] = $fr;
		$fr = "";
		for($i = 0; $i < count($_POST['sport']); $i++){
			$fr .= "('" . $id . "', '" . $_POST['sport'][$i] . "'),";
		}
		if($fr == ""){
			$stmt_3 = true;
		} else {
			$fr = substr($fr,0, strlen($fr)-1);
			$stmt_3 = $pdo->prepare("insert into user_sport (id_user, id_sport) value " . $fr)->execute();
		}
		$data['3'] = $fr;
		if($stmt_1 && $stmt_2 && $stmt_3){
			if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
				$arquivo = $_FILES['imagem'];
				$extensao = strtolower(end(explode('.', $arquivo['name'])));
				$name = "img_" . $id . "." . $extensao;
				$destino = $path_users . $name;
				move_uploaded_file($arquivo['tmp_name'], $destino);
				$resizeObj = new imageLib($destino);
				$resizeObj -> resizeImage(500, 500, 'crop');
				$resizeObj -> saveImage($destino, 100);
				$pdo->prepare("update users set path = '" . $name . "' where id_user = $id")->execute();
			} else {
				$resizeObj = false;
			}

			if($resizeObj){
				$pdo->commit();
				$data["result"] = "ok";
				$data["msg"] = "Inscrição realizada com sucesso";
				//$data['page'] = '';
			} else {
				$pdo->rollBack();
				$data["result"] = "error";
				$data['msg'] = "Sem imagem";
			}
		}else{
			$pdo->rollBack();
			$data["result"] = "error";
			$data['msg'] = "Algum dado incorreto";
		}
		if($stmt_1) $data['st1'] = true;
		if($stmt_2) $data['st2'] = true;
		if($stmt_3) $data['st3'] = true;

		echo json_encode($data);
		exit();
	}
?>