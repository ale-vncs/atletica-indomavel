<?php
	//echo json_encode($_POST['type']);
	require ('check_session.php');

	$data = array();
	$diretorio = $path_partner;
	$type = $_POST['type'];

	if($type == "getPartner"){
		$sql = $pdo->prepare("select * from partner");

		if($sql->execute()){
			$g = 0;
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		}

		if($data == []){
			$data["result"] = "error";
			$data["msg"] = "Nenhuma informação encontrada";
		} else {
			$data["length"] = count($data);
			$data["result"] = "ok";
			$data["msg"] = "";
		}
	} else if($type == "setPartner"){

		$partnerName = $_POST['name'];
		$partnerAbout = $_POST['about'];

		if(!isset($_FILES['imagem'])){
			$data["result"] = "error";
			$data["msg"] = "Sem imagem";
			echo json_encode($data);
			exit();
		}

		$arquivo = $_FILES['imagem'];
		$extensao = strtolower(end(explode('.', $arquivo['name'])));
		$name = "img_partner_" . ($_POST['idLast'] + 1) . "." . $extensao;
		$destino = $path_partner . $name;
		move_uploaded_file($arquivo['tmp_name'], $destino);
		$resizeObj = new imageLib($destino);
		$resizeObj -> resizeImage(500, 500, 'auto');
		$resizeObj -> saveImage($destino, 100);


		$sql = $pdo->prepare("insert into partner (name, about, path) value ('$partnerName','$partnerAbout','$name')");


		if($sql->execute()){
			$data['result'] = "ok";
			$data['msg'] = "Parceiro adicionado";
			setLog('partner','add','Parceiro adicionado');
		} else {
			$data['result'] = "error";
			$data['msg'] = "Erro ao comprimmir imagem";
			unlink($diretorio.$name);
		}
	} else if($type == "delPartner"){
		$name = $_POST['name'];
		if(unlink($diretorio.$name)){
			$pdo->prepare("delete from partner where path='$name'")->execute();
			setLog('partner','rem','Parceiro removido');
			$data['result'] = "ok";
			$data['msg'] = "Parceiro apagado";
		} else {
			$data['result'] = "error";
			$data['msg'] = "Erro ao apagar parceiro";
		}
	} else if($type == "updatePartner"){
		$name = $_POST['imagem'];
		$partnerId = $_POST['id'];
		$partnerAbout = $_POST['about'];
		$partnerName = $_POST['name'];

		if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
			unlink($diretorio.$name);
			$arquivo = $_FILES['imagem'];
			$extensao = strtolower(end(explode('.', $arquivo['name'])));
			$name = "img_partner_" . $partnerId . "." . $extensao;
			$destino = $path_partner . $name;
			move_uploaded_file($arquivo['tmp_name'], $destino);
			$resizeObj = new imageLib($destino);
			$resizeObj -> resizeImage(500, 500, 'auto');
			$resizeObj -> saveImage($destino, 100);
		} else {
			$resizeObj = true;
		}

		$sql = $pdo->prepare("update partner set name='$partnerName', about='$partnerAbout',path='$name' where id_partner = '$partnerId'");

		if($sql->execute() && $resizeObj){
			$data['result'] = "ok";
			$data['msg'] = "Parceiro atualizado";
			setLog('partner','mod','Parceiro atualizado');
		} else {
			$data['result'] = "error";
			$data['msg'] = "Erro ao atualizar parceiro";
		}
		$data['fr'] = $name . " - " . $partnerId;
	}

	echo json_encode($data);
