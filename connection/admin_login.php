<?php
	include "check_session.php";

	session_start();

	$data = array(); //Variavel padrão de retorno de informação do banco

	define('TENTATIVAS_ACEITAS', 5); // Constante com a quantidade de tentativas aceitas
	define('MINUTOS_BLOQUEIO', 30); // Constante com a quantidade minutos para bloqueio

	$id_user = "";
	$registry = (isset($_POST['registry'])) ? $_POST['registry'] : '';
	$password = (isset($_POST['password'])) ? $_POST['password'] : '';

	if (empty($registry)){ //Verifica se o nuemro de matricula está vazio
		$data["result"] = "error";
		$data["msg"] = "Numero de matricula vazio";
		echo json_encode($data);
		exit();
	}

	if (empty($password)){ //Verifica se a senha está vazia
		$data["result"] = "error";
		$data["msg"] = "Senha em branco";
		echo json_encode($data);
		exit();
	}

	$sql = $pdo->prepare("select id_user from users where registry = '$registry' and is_admin = true");

	if($sql->execute()){
		while($row = $sql->fetch(PDO::FETCH_ASSOC)){
			foreach($row as $field => $value){
				$id_user = $value;
			}
		}
	}
	if($id_user == ""){
		$data["msg"] = "Usuário ou senha incorreta";
		$data["result"] = "error";
		session_destroy();
		echo json_encode($data);
		exit();
	}

	$sql = $pdo->prepare("select user_blocked, attempts, (timediff(now(), blocked_date)/60) as time from user_attempts_login where id_user = '$id_user'");

	if($sql->execute()){
		while($row = $sql->fetch(PDO::FETCH_ASSOC)){
			foreach($row as $field => $value){
				$data[$field] = $value;
			}
		}
	}

	if($data == []){
		$data["result"] = "error";
		$data["msg"] = "Usuario não existe";
		echo json_encode($data);
		exit();
	} else if ($data["user_blocked"] && $data["time"] <= MINUTOS_BLOQUEIO){
		$data["result"] = "error";
		$data["msg"] = "Usuario bloqueado por " . floor(MINUTOS_BLOQUEIO - $data["time"]) . " minutos";
		echo json_encode($data);
		exit();
	} else if ($data["user_blocked"]){
		$data["attempts"] = 0;
		$pdo->prepare("update user_attempts_login set attempts = 0, user_blocked = false, blocked_date = null where id_user = '$id_user'")->execute();
	}


	$sql = $pdo->prepare("select  users.id_user,
       										users.path,
       										if((select users.id_type = 1),1,0) as high,
       										admin.password
                                    from user_admin admin
                                    inner join users users on users.id_user = admin.id_user
                                    where users.id_user = '$id_user' and admin.password='$password'"
    );

	if($sql->execute()){
		while($row = $sql->fetch(PDO::FETCH_ASSOC)){
			foreach($row as $field => $value){
				$data[$field] = $value;
			}
		}
	}

	if($data["password"] != $password){
		$tentativas = $data["attempts"] + 1;
		if ($data["attempts"] >= TENTATIVAS_ACEITAS){
			$pdo->prepare("update user_attempts_login set attempts = '$tentativas', user_blocked = true, blocked_date = current_timestamp() where id_user = '$id_user'")->execute();
			$data["msg"] = "Usuário bloqueado por muitas tentativas";
		} else {
			$pdo->prepare("update user_attempts_login set attempts = '$tentativas' where id_user = '$id_user'")->execute();
			$data["msg"] = "Usuário ou senha incorreta";
		}
		$data["result"] = "error";
		session_destroy();
	}else{
		$data["result"] = "ok";
		$_SESSION['data']['id_user'] = $data['id_user'];
		$_SESSION['data']['high'] = $data['high'];
		$_SESSION['data']['password'] = $data['password'];
		$_SESSION['data']['path'] = $data['path'];
		setLog('home','acc','Usuário logado');
		$pdo->prepare("update user_attempts_login set attempts = 0, user_blocked = false, blocked_date = null where id_user = '$id_user'")->execute();
	}

	echo json_encode($data);
?>