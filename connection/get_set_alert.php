<?php
	require ("check_session.php");

	$data = array();
	$type = $_POST['type'];

	if($type == "getAlert"){
		$sql = $pdo->prepare("select *, date_format(expiration_date, '%d/%m/%Y') as expiration_date from alert order by priority");
		$g =0;
		if($sql->execute()){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$g][$field] = $value;
				}
				$g++;
			}
		}

		if($data == []){
			$data["result"] = "error";
			$data["msg"] = "Nenhum aviso";
		}
	} else if($type == "addAlert") {
		$titleAlert = isset($_POST['titleAlert']) ? $_POST['titleAlert'] : null;
		$messageAlert = isset($_POST['messageAlert']) ? $_POST['messageAlert'] : null;
		$priorityAlert = $_POST['priorityAlert'];
		$expirationDate = (isset($_POST['year']) && isset($_POST['mouth']) && isset($_POST['day'])) ? $_POST['year'] . "-" . $_POST['mouth'] . "-" . $_POST['day'] : null;

/*		$idAlert = $pdo->prepare("select id_alert from alert order by id_alert desc");
		$idAlert->execute();
		$id = $idAlert->fetch(PDO::FETCH_ASSOC)['id_alert'];
		$id++;*/
		$pdo->beginTransaction();

		$sql = $pdo->prepare("insert into alert (id_user, priority, title, text, expiration_date) value (" . $_SESSION['data']['id_user'] . ", '$priorityAlert', '$titleAlert', '$messageAlert', '$expirationDate')")->execute();

		if($sql){
			if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
				$arquivo = $_FILES['imagem'];
				$extensao = strtolower(end(explode('.', $arquivo['name'])));
				$name = "alert_" . $pdo->lastInsertId() . "." . $extensao;
				$destino = $path_alert . $name;
				move_uploaded_file($arquivo['tmp_name'], $destino);
				$resizeObj = new imageLib($destino);
				$resizeObj -> resizeImage(500, 500,'auto');
				$resizeObj -> saveImage($destino, 100);
				$pdo->prepare("update alert set path='".$name."' where id_alert=".$pdo->lastInsertId())->execute();
			} else {
				$resizeObj = true;
			}

			if($resizeObj){
				$data["result"] = "ok";
				$data["msg"] = "Alerta adicionado";
				setLog('alert','add','Alerta adicionado');
				$pdo->commit();
			} else {
				$pdo->rollBack();
				$data["result"] = "error";
				$data["msg"] = "Algum erro ocorreu";
			}

		} else {
			$pdo->rollBack();
			$data["result"] = "error";
			$data["msg"] = "Algum erro ocorreu";
		}
	} else if($type == "updateAlert"){
		if(isset($_POST['titleAlert'])){
			$set = "title='" . $_POST['titleAlert'] ."'";
		}
		if(isset($_POST['messageAlert'])){
			if(!is_null($set)) $set .= ",";
			$set .= "text='" . $_POST['messageAlert'] . "'";
		}
		if(isset($_POST['priorityAlert'])){
			if(!is_null($set)) $set .= ",";
			$set .= "priority=" . $_POST['priorityAlert'];
		}
		if(isset($_POST['expirationDate'])){
			if(empty($_POST['expirationDate'])){
				$_POST['expirationDate'] = null;
			} else {
				$date = convertDateBrToDB($_POST['expirationDate']);

				if(date("Y-m-d") > $date){
					$data['result'] = "error";
					$data['msg'] = "A data deve ser maior que atual";
					echo json_encode($data);
					exit();
				}
			}
			if(!is_null($set)) $set .= ",";
				$set .= "expiration_date='".$date."'";
		}

		if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
			$arquivo = $_FILES['imagem'];
			$extensao = strtolower(end(explode('.', $arquivo['name'])));
			$name = "alert_" . $_POST['id_alert'] . "." . $extensao;
			$destino = $path_alert . $name;
			move_uploaded_file($arquivo['tmp_name'], $destino);
			$resizeObj = new imageLib($destino);
			$resizeObj -> resizeImage(500, 500,'auto');
			$resizeObj -> saveImage($destino, 100);
			if(!is_null($set)) $set .= ",";
			$set .= "path='".$name."'";
		} else if(isset($_POST['remImg']) && $_POST['remImg'] == "true"){//file_exists($path_users . $_SESSION['data']['path'])
			$idAlert = $pdo->prepare("select path from alert where id_alert=".$_POST['id_alert']);
			$idAlert->execute();
			$id = $idAlert->fetch(PDO::FETCH_ASSOC)['path'];
			unlink($path_alert . $id);
			$resizeObj = true;
			if(!is_null($set)) $set .= ",";
			$set .= "path=null";
		} else {
			$resizeObj = true;
		}

		$sql = $pdo->prepare("update alert set " . $set . " where id_alert = " . $_POST['id_alert']);

		if($sql->execute()){
			$data["result"] = "ok";
			$data["msg"] = "Alerta atualizado";
			setLog('alert','mod','Alerta atualizado');
		} else {
			$data["result"] = "error";
			$data["msg"] = "Algum erro ocorreu";
		}
	} else if($type == "deleteAlert"){
		$idAlert = $pdo->prepare("select path from alert where id_alert=".$_POST['id_alert']);
		$idAlert->execute();
		$id = $idAlert->fetch(PDO::FETCH_ASSOC)['path'];
		unlink($path_alert . $id);
		$sql = $pdo->prepare("delete from alert where id_alert=" . $_POST['id_alert']);

		if($sql->execute()){
			$data["result"] = "ok";
			$data["msg"] = "Alerta deletado";
			setLog('alert','rem','Alerta removido');
		} else {
			$data["result"] = "error";
			$data["msg"] = "Não foi possivel deletar";
		}
	}else {
		$data["result"] = "error";
		$data["msg"] = "Parametro type não enviado";
	}

	echo json_encode($data);