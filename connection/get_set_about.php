<?php
	include('check_session.php');

	$data = array();

	if (count($_POST) > 0){

		$set = null;
		if(isset($_POST['about'])){
			$set = "about_text = '" . $_POST['about'] . "'";
		}
		if(isset($_POST['name'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "name = '" . $_POST['name'] . "'";
		}
		if(isset($_POST['title'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "title_index = '" . $_POST['title'] . "'";
		}
		if(isset($_POST['sub_title'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "sub_title_index = '" . $_POST['sub_title'] . "'";
		}
		if(isset($_POST['association'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "enabled_association = 1";
		} else {
			if(!is_null($set)){ $set .= ','; }
			$set .= "enabled_association = 0";
		}

		if(isset($_POST['date_expiration'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "date_expiration = date_format(str_to_date('" . $_POST['date_expiration'] . "','%d/%m/%Y'),'%Y-%m-%d')";
		}

		if(isset($_POST['color-primary'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "color_primary = '" . $_POST['color-primary'] . "'";
		}

		if(isset($_POST['color-secondary'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "color_secondary = '" . $_POST['color-secondary'] . "'";
		}

		if(isset($_POST['color-text'])){
			if(!is_null($set)){ $set .= ','; }
			$set .= "color_text = '" . $_POST['color-text'] . "'";
		}

		if($set == null) exit();

		$sql = $pdo->prepare("update about set " . $set);


		if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
			$arquivo = $_FILES['imagem'];
			$extensao = strtolower(end(explode('.', $arquivo['name'])));
			if ($extensao != "png") {
				$data['result'] = "error";
				$data['msg'] = "O formato da imagem tem que ser PNG";
				echo json_encode($data);
				return;
			}

			$destino = $path_default . "logo.png";
			move_uploaded_file($arquivo['tmp_name'], $destino);
			$resizeObj = new imageLib($destino);
			$resizeObj -> saveImage($path_default . "logo.png", 60);
			$resizeObj -> resizeImage(500, 500, 'auto');
			$resizeObj -> saveImage($path_default . "loading.png", 60);
		} else {
			$resizeObj = true;
		}


		if($sql->execute() && $resizeObj){
			$data['result'] = "ok";
			setLog('about','mod','Informações alteradas');
			$data['msg'] = "Informações alteradas";
		}else {
			$data['result'] = "error";
			$data['msg'] = "Erro ao alterar infomações";
		}
			$data['df'] = $_POST['color-primary'];


	} else {

		$sql = $pdo->prepare("select * from about");

		if($sql->execute()){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$field] = $value;
					if($field == "date_expiration"){
						$data[$field] = convertDateDBToBr($value);
					}
				}
			}
		}

		if($data == []){
			$data["result"] = "error";
		}else{
			$data["result"] = "ok";
		}

	}


	echo json_encode($data);
