<?php
	require_once 'check_session.php';

	if(count($_POST) > 0){
		$set_1 = $set_2 = $set_3 = null;
		if(!isset($_POST['password_current'])){
			setLog('account','mod','Tentativa de alterar dados');
			echo json_encode(array("result"=>"error","msg"=>"Digite sua senha!"));
			exit();

		} else if($_POST['password_current'] != $_SESSION['data']['password']){
			setLog('account','mod','Tentativa de alterar dados');
			echo json_encode(array("result"=>"error","msg"=>"Senha atual incorreta!"));
			exit();
		}
		if(isset($_POST['about'])){
			$set_1 = "about = " . "'" . $_POST['about'] . "'";
		}
		if(isset($_POST['password_new']) && !empty($_POST['password_new'])){
			if(!is_null($set_1)){ $set_1 .= ','; }
			$set_1 .= "password = '" . $_POST['password_new'] . "'";
		}

		if(isset($_POST['registry'])){
			$set_2 = "registry = '" . $_POST['registry'] . "'";
		}

		if(isset($_POST['name'])){
			if(!is_null($set_2)){ $set_2 .= ",";}
			$set_2 .= "name = '" . $_POST['name'] . "'";
		}

		if(isset($_POST['email'])){
			if(!is_null($set_2)){ $set_2 .= ",";}
			$set_2 .= "email = '" . $_POST['email'] . "'";
		}

		if(isset($_POST['birthday_date'])){
			if(!is_null($set_2)){ $set_2 .= ",";}
			$set_2 .= "birthday_date = date_format(str_to_date('" . $_POST['birthday_date'] . "', '%d/%m/%Y'),'%Y-%m-%d')";
		}
		if(isset($_POST['gender'])){
			if(!is_null($set_2)){ $set_2 .= ",";}
			$set_2 .= "id_gender = '" . $_POST['gender'] . "'";
		}
		if(isset($_POST['class'])){
			if(!is_null($set_2)){ $set_2 .= ",";}
			$set_2 .= "id_course = '" . $_POST['course'] . "'";
		}

		for($i = 0; $i < count($_POST['sport']); $i++){
				$set_3 .= "('" . $_SESSION['data']['id_user'] . "', '" . $_POST['sport'][$i] . "'),";
		}
		$set_3 = substr($set_3,0, strlen($set_3)-1);


		$pdo->beginTransaction();
		$pdo->prepare("delete from user_sport where id_user = '" . $_SESSION['data']['id_user'] . "'")->execute();
		$sql_1 = $pdo->prepare("update user_admin set " . $set_1 . " where id_user = " . $_SESSION['data']['id_user'])->execute();
		$sql_2 = $pdo->prepare("update users set " . $set_2 . " where id_user = " . $_SESSION['data']['id_user'])->execute();
		$sql_3 = ($set_3 != null ? $pdo->prepare("insert into user_sport (id_user, id_sport) value " . $set_3)->execute() : 1);

		if($sql_1 && $sql_2 && $sql_3){

			if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['name'])){
				unlink($path_users . $_SESSION['data']['path']);

				$arquivo = $_FILES['imagem'];
				$extensao = strtolower(end(explode('.', $arquivo['name'])));
				$name = "img_" . $_SESSION['data']['id_user'] . "." . $extensao;
				$destino = $path_users . $name;
				move_uploaded_file($arquivo['tmp_name'], $destino);
				$_SESSION['data']['path'] = $name;
				$resizeObj = new imageLib($destino);
				$resizeObj -> resizeImage(500, 500, 'crop');
				$resizeObj -> saveImage($destino, 100);
			} else if(isset($_POST['remImg']) && $_POST['remImg'] == "true"){//file_exists($path_users . $_SESSION['data']['path'])
				unlink($path_users . $_SESSION['data']['path']);
				$resizeObj = true;
				$name = $unknown_people;
			} else {
				$resizeObj = true;
				$name = $_SESSION['data']['path'];
			}

			if($resizeObj){
				$pdo->commit();
				$_SESSION['data']['password'] = $_POST['password_new'];
				$data['result'] = "ok";
				setLog('account','mod','Dados alterados');
				$pdo->prepare("update users set path = '$name' where id_user = '" . $_SESSION['data']['id_user'] . "'")->execute();
				$data['msg'] = "Informações alteradas";
			} else {
				$pdo->rollBack();
				$data['result'] = "error";
				$data['msg'] = "Erro ao alterar infomações";
			}

		}else {
			$pdo->rollBack();
			$data['result'] = "error";
			$data['msg'] = "Erro ao alterar infomações";
		}
		$data['sql_result'] = $sql_1 . " - " . $sql_2 . " - " . $sql_3;
		$data['sql'] = $set_3;
	} else {
		$sql = $pdo->prepare("select  users.registry, 
                                            users.name,
       										users.path,
       										(select gender from gender where id_gender = users.id_gender) as gender,
       										users.email,
       										ifnull(group_concat((select name from sport where id_sport = usp.id_sport group by name)), 'Sem Esporte') as sport,
       										ifnull((select name from course where id_course = users.id_course group by name), 'Sem Curso') as course,
       										date_format(users.birthday_date,'%d/%m/%Y') as birthday_date,
       										admin.about,
                                            (select name from admin_type where id_type = admin.id_type) as status,
       										if((select users.id_type = 1),1,0) as high
                                    from user_admin admin
                                    inner join users users on users.id_user = admin.id_user
                                    left join user_sport usp on users.id_user = usp.id_user
                                    where users.id_user = " . $_SESSION['data']['id_user']
		);

		if($sql->execute()){
			while($row = $sql->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$field] = $value;
				}
			}
		}

	}


	echo json_encode($data);