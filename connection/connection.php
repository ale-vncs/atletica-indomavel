<?php
	error_reporting(0);
	session_cache_expire(5);
	date_default_timezone_set('America/Belem');

	/*
	 * Constantes de parâmetros para configuração da conexão
	 */


	$path_default = "../img/";
	$path_gallery = $path_default."gallery/";
	$path_users = $path_default."users/";
	$path_partner = $path_default."partner/";
	$path_alert = $path_default."alert/";

	$unknown_people = "unknown_people.png";
	$unknown_partner = "unknown_partner.png";

	$host_default = "neopolaris.com";
	$host_debug = "localhost:8085";

	if(!is_dir($path_default)){	mkdir($path_default, 0777, true); }
	if(!is_dir($path_gallery)){	mkdir($path_gallery, 0777, true); }
	if(!is_dir($path_users)){	mkdir($path_users, 0777, true); }
	if(!is_dir($path_partner)){	mkdir($path_partner, 0777, true); }
	if(!is_dir($path_alert)){	mkdir($path_alert, 0777, true); }

	if ($_SERVER['HTTP_HOST'] == $host_debug){
		define('HOST',"localhost");//"mysql.hostinger.com.br";
		define('DBNAME',"u406455635_atlet");
		define('USER',"root");
		define('PASSWORD',"253726867");
	} else {
		define('HOST',"sql131.main-hosting.eu");//"mysql.hostinger.com.br";
		define('DBNAME',"u406455635_atlet");
		define('USER',"u406455635_atlet");
		define('PASSWORD',"253726867");

	}

	function sendError($msg){
		echo json_encode(array("page"=>"../../atletica-indomavel/error.html?error=" . $msg));
		exit();
	}

	class Conexao {

		/*
		 * Atributo estático para instância do PDO
		 */
		private static $pdo;

		/*
		 * Escondendo o construtor da classe
		 */
		private function __construct() {
			//
		}

		/*
		 * Método estático para retornar uma conexão válida
		 * Verifica se já existe uma instância da conexão, caso não, configura uma nova conexão
		 */
		public static function getInstance() {
			if (!isset(self::$pdo)) {
				try {
					$opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_PERSISTENT => TRUE);
					self::$pdo = new PDO("mysql:host=" . HOST . "; dbname=" . DBNAME . "; charset=utf8;", USER, PASSWORD);
				} catch (PDOException $e) {
					//print "Erro: " . $e->getMessage();
					sendError("Error ao tentar acessar banco");
				}
			}
			return self::$pdo;
		}
	}
