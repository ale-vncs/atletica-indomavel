<?php
include('check_session.php');


$type = $_POST['type'];

$data = array();

if ($type == "action"){
	$fr = "select id_action as id, action as name from action";
} else if ($type == "section"){
	$fr = "select id_section as id, section as name from section";
} else if ($type == "course"){
	$fr = "select id_course as id, name from course";
} else if ($type == "sport"){
	$fr = "select id_sport as id, name from sport";
} else if ($type == "user_type"){
	$fr = "select id_type as id, name from user_type";
} else if ($type == "admin_type"){
	$fr = "select id_type as id, name from admin_type";
}else if ($type == "gender"){
	$fr = "select id_gender as id, gender as name from gender";
} else if($type == "member") {
	$fr = "select id_user, name from users where is_admin = 0 order by name asc";
}

$sql = $pdo->prepare($fr);


$g = 0;
if($sql-> execute()){
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		foreach($row as $field => $value){
			$data[$g][$field] = $value;
		}
		$g = ($g + 1);
	}
}

if($data == []){
	$data["result"] = "error";
}else{
	$data["length"] = count($data);
	$data["result"] = "ok";
}

echo json_encode($data);
?>