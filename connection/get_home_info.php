<?php

	include ("check_session.php");

	isset($_POST['byDate']) ? $dateFilter = "%d/%m/%Y" : $dateFilter = "%m/%Y";


	$data = array();

	$stmt = $sql = array();

	$sql_name = array("count","course","gender","sport","visit");
	$sql[] = "select (select count(*) from partner) as count_partner, (select count(*) from users) as count_users, (select count(*) from alert) as count_alert;";
	$sql[] = "select c.name, count(users.id_course) as count from users right join course c on users.id_course = c.id_course group by c.name;";
	$sql[] = "select g.gender, count(users.id_gender) as count from users right join gender g on users.id_gender = g.id_gender group by g.gender;";
	$sql[] = "select s.name, count(user_sport.id_sport) as count from user_sport inner join sport s on user_sport.id_sport = s.id_sport group by s.name;";
	$sql[] = "select date_format(date_visit, '$dateFilter') as name, COUNT(date_format(date_visit, '$dateFilter')) as count from visit_site group by date_format(date_visit, '$dateFilter') order by date_visit limit 12";
	$c = 0;
	for($i = 0; $i < count($sql); $i++){
		$stmt[$i] = $pdo->prepare($sql[$i]);
		if($stmt[$i]->execute()){
			$g = 0;
			while($row = $stmt[$i]->fetch(PDO::FETCH_ASSOC)){
				foreach($row as $field => $value){
					$data[$sql_name[$c]][$g][$field] = $value;
				}
				$g++;
			}
			$data[$sql_name[$c]]['length'] = $g;
			$c++;
		}
	}

	echo json_encode($data);