<?php
include('check_session.php');

$action = $section = $registry = $name = $date = $limit = "";

if(isset($_POST['action']) && $_POST['action'] != 0)
    $action = " and a.id_action = " . $_POST['action'];

if (isset($_POST['section']) && $_POST['section'] != 0)
    $section = " and s.id_section = " . $_POST['section'];

if (isset($_POST['registry']) && !empty($_POST['registry']))
    $registry = " and l.registry like '%" . $_POST['registry'] . "%'";

if (isset($_POST['date']) && !empty($_POST['date']))
    $date = " and date_format(l.date,'%d/%m/%Y') = '" . $_POST['date']  . "'";

if (isset($_POST['name']) && !empty($_POST['name']))
    $name = " and u.name like '%" . $_POST['name'] . "%'";

if (isset($_POST['limit']))
	$limit = " limit " . $_POST['limit'];

$data = array();



$fr = "select distinct 	u.registry,
                        (select name from users where registry = u.registry) as name,
                        (select section from section where id_section = s.id_section) as section,
                        (select action from action where id_action = a.id_action) as action,
                        l.message,
                        (date_format(l.date,'%d/%m/%Y -- %T')) as date
                from log
                inner join log l
                inner join action a on l.id_action = a.id_action
                inner join section s on l.id_section = s.id_section
                inner join users u on l.id_user = u.id_user
                where 0=0" . $action . $registry . $section . $date . $name .
				" order by l.id_log desc" . $limit

;

$sql = $pdo->prepare($fr);

$g = 0;
if($sql->execute()){
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		foreach($row as $field => $value){
			$data[$g][$field] = $value;
		}
		$g = ($g + 1);
	}
}


if($data == []){
    $data["result"] = "error";
    $data['fe'] = $fr;
}else{
    $data["length"] = count($data);
    $data["result"] = "ok";
}

echo json_encode($data);
?>